# Spring RESTClothing
***
This project is a sample implementation of a fictional online clothing store. 
It's a showcase for bringing different Spring eco-system technologies together 
to implement a REST web service. The application uses [HAL](http://stateless.co/hal_specification.html) 
as the primary representation format.

## Quickstart
***
From the command line do:

```bash
$ git clone https://ArtyomFyodorov@bitbucket.org/ArtyomFyodorov/spring-restclothing.git
$ cd spring-restclothing
$ mvn clean package
$ java -jar target/*.jar
```
The application ships with the [HAL browser](https://github.com/mikekelly/hal-browser) embedded, 
so simply browsing to [http://localhost:8080/browser/index.html](http://localhost:8080/browser/index.html) 
will allow you to explore the web service.

## IDE setup
***
For the usage inside an IDE do the following:

### Eclipse

1. Make sure you have an Eclipse with m2e installed (preferably [STS](http://spring.io/sts)).
2. Follow to the [Lombok setup page](https://projectlombok.org/setup/eclipse).
3. Restart Eclipse.
4. Import the checked out code through *File > Import > Existing Maven Project…*

### IntelliJ IDEA

1. Follow to the [Lombok setup page](https://projectlombok.org/setup/intellij).
2. Make sure that annotation processing is enabled 
([Configuring Annotation Processing](https://www.jetbrains.com/help/idea/configuring-annotation-processing.html)).
3. Import the checked out code through *File > Open...*

## Used Projects
***
- [Spring Boot](http://github.com/spring-projects/spring-boot)
- [Spring (MVC)](http://github.com/spring-projects/spring-framework)
- [Spring Data JPA](http://github.com/spring-projects/spring-data-jpa)
- [Spring Data REST](http://github.com/spring-projects/spring-data-rest)
- [Spring HATEOAS](http://github.com/spring-projects/spring-hateoas)
- [Spring Plugin](http://github.com/spring-projects/spring-plugin)
- [Spring Security](http://github.com/spring-projects/spring-security)
- [Spring REST Docs](https://github.com/spring-projects/spring-restdocs)

### Miscellaneous

- [Lombok](http://projectlombok.org)

## Security
***
This sample contains additional configuration to secure the service using 
Spring Security and [HTTP Basic authentication](https://en.wikipedia.org/wiki/Basic_access_authentication).

If you run the sample you should be able to follow this interaction 
(I am using [cURL](https://github.com/curl/curl) here).

```bash
$ curl http://localhost:8080/customers/search/current-customer -i
HTTP/1.1 401
…
WWW-Authenticate: Basic realm="Spring Restclothing"
```

You can authenticate using the default credentials:

```text
Administrator:
    username: admin@clothing.com
    password: admin
Customer:
    username: duchess.mock@mail.com
    password: Strong!
```

```bash
$ curl http://localhost:8080/customers/search/current-customer -i -u 'username:password'
HTTP/1.1 200
…
Content-Type: application/hal+json;charset=UTF-8
```