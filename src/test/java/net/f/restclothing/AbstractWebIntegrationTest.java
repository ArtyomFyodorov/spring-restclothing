package net.f.restclothing;

import org.junit.Before;
import org.junit.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.hypermedia.LinksSnippet;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

/**
 * @author Artyom Fyodorov
 */
public abstract class AbstractWebIntegrationTest extends AbstractIntegrationTest {

    public final @Rule JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");
    protected final LinksSnippet curiesLink = links(linkWithRel("curies").description("CUR-ies."));
    protected MockMvc mockMvc;
    private @Autowired WebApplicationContext webAppCtx;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webAppCtx)
                .apply(springSecurity())
                .apply(documentationConfiguration(this.restDocumentation))
                .defaultRequest(MockMvcRequestBuilders.get("/"))
                .build();
    }
}
