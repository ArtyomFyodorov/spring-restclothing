package net.f.restclothing.business;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.f.restclothing.business.orders.invoice.Invoice;
import net.f.restclothing.business.orders.order.LineItem;
import net.f.restclothing.business.orders.order.Order;
import net.f.restclothing.business.orders.product.Product;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class JsonSerializationTest {

    private ObjectMapper mapper;
    private Order order;
    private LineItem lineItem;
    private Invoice invoice;
    private Product product;

    @Before
    public void setUp() throws Exception {
        this.mapper = new ObjectMapper();

        this.order = new Order();
        ReflectionTestUtils.setField(this.order, "totalAmount", BigDecimal.valueOf(42.42));

        this.lineItem = new LineItem();
        ReflectionTestUtils.setField(this.lineItem, "unitPrice", BigDecimal.valueOf(42.42));
        ReflectionTestUtils.setField(this.lineItem, "amount", BigDecimal.valueOf(42.42));
        ReflectionTestUtils.setField(this.lineItem, "quantity", 1);

        this.invoice = new Invoice();
        ReflectionTestUtils.setField(this.invoice, "amountOfInvoice", BigDecimal.valueOf(42.42));

        this.product = new Product();
        ReflectionTestUtils.setField(this.product, "unitPrice", BigDecimal.valueOf(42.42));
    }

    @Test
    public void big_decimal_serialization_test() throws Exception {

        final String orderResult = this.mapper.writeValueAsString(order);
        assertThat(orderResult, containsString("\"totalAmount\":\"$42.42\""));

        final String lineItemResult = this.mapper.writeValueAsString(lineItem);
        assertThat(lineItemResult, containsString("\"unitPrice\":\"$42.42\""));
        assertThat(lineItemResult, containsString("\"amount\":\"$42.42\""));

        final String invoiceResult = this.mapper.writeValueAsString(invoice);
        assertThat(invoiceResult, containsString("\"amountOfInvoice\":\"$42.42\""));

        final String productResult = this.mapper.writeValueAsString(product);
        assertThat(productResult, containsString("\"unitPrice\":\"$42.42\""));
    }

}
