package net.f.restclothing.business;

import net.f.restclothing.business.customers.address.Address;
import net.f.restclothing.business.customers.address.Country;
import net.f.restclothing.business.customers.creditcard.CreditCard;
import net.f.restclothing.business.customers.creditcard.CreditCardType;
import net.f.restclothing.business.customers.customer.Customer;
import net.f.restclothing.business.customers.customer.CustomerRepository;
import net.f.restclothing.business.orders.order.LineItem;
import net.f.restclothing.business.orders.order.Order;
import net.f.restclothing.business.orders.product.Product;
import net.f.restclothing.business.orders.product.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.time.Month;
import java.time.Year;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Artyom Fyodorov
 */
@Component
public class TestDataProducer {
    /**
     * The email address of the administrator stored in the database.
     */
    public static final String ADMIN_EMAIL = "admin@clothing.com";
    /**
     * The email address of the customer stored in the database.
     */
    public static final String CUSTOMER_EMAIL = "richard.roe@mail.com";
    /**
     * The password of the customer stored in the database.
     */
    public static final String CUSTOMER_PASSWORD = "Qwerty123";

    @Autowired CustomerRepository customerRepository;
    @Autowired ProductRepository productRepository;


    public static Customer getCustomer() {
        Address shippingAddress = new Address("15 Furshtatskaya St.",
                "St. Petersburg", "", Country.RUSSIAN_FEDERATION, 191028);
        CreditCard creditCard = CreditCard.create("1029-3847-5610-2938",
                "John Doe", Month.JANUARY, Year.now().plusYears(1), CreditCardType.VISA);
        Customer customer = new Customer("John", "Doe",
                "john.doe@mail.com", "Qwerty123");
        customer.addAddress(shippingAddress);
        customer.setCreditCard(creditCard);

        return customer;
    }

    public static Order getOrder(Long customerNumber,
                                 Address shippingAddress,
                                 Product product,
                                 int quantity,
                                 double tax) {
        Order order = Order.create(customerNumber, shippingAddress);
        order.addLineItem(LineItem.create(product, quantity));

        return order;
    }

    public static Product getProduct() {
        return getProductStream().findFirst().get();
    }

    public static Stream<Product> getProductStream() {
        return Stream.of(
                new Product("4 Pack Bolter Men's Everyday Cotton Blend Short Sleeve T-shirt, Small",
                        "B06XSK6ST4", 26.99),
                new Product("Womens Street Style Feather Pattern T-shirts Casual Loose Top Tee Shirts, Large, Small",
                        "B072LRZ8KV", 13.45),
                new Product("Southern Marsh Authentic Tee, Medium",
                        "B01N5VATF3", 30.),
                new Product("Women Summer Pocket Short Sleeve Shirt T-shirt Casual Blouse Tops, Medium",
                        "B06XGVW5XR", 7.69));
    }

    /**
     * @return customer by default email address.
     * See {@link TestDataProducer#CUSTOMER_EMAIL} constant.
     */
    public Customer getCustomerEntity() {
        return getCustomerEntityByEmail(CUSTOMER_EMAIL);
    }

    public Customer getCustomerEntityByEmail(String email) {
        Assert.notNull(email, "Email may not be null");
        Optional<Customer> maybeCustomer = customerRepository.findByEmail(CUSTOMER_EMAIL);
        assertTrue(maybeCustomer.isPresent());

        return maybeCustomer.get();
    }

    public Product getProductEntity() {
        Product product =
                productRepository.save(getProduct());
        assertNotNull(product.getId());

        return product;
    }
}
