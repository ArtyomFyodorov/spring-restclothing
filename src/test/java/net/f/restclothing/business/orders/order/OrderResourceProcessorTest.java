package net.f.restclothing.business.orders.order;

import net.f.restclothing.business.orders.invoice.InvoiceLinks;
import net.f.restclothing.business.payments.payment.PaymentLinks;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author Artyom Fyodorov
 */
@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class OrderResourceProcessorTest {

    private @Mock InvoiceLinks invoiceLinks;
    private @Mock PaymentLinks paymentLinks;
    private @Mock OrderLinks orderLinks;

    private Link invoiceLink;
    private Link paymentLink;
    private Link cancellationLink;
    private OrderResourceProcessor processor;

    @Before
    public void set_up() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        ServletRequestAttributes requestAttributes = new ServletRequestAttributes(request);
        RequestContextHolder.setRequestAttributes(requestAttributes);

        this.invoiceLink = new Link(InvoiceLinks.INVOICE, InvoiceLinks.INVOICE_REL);
        this.paymentLink = new Link(PaymentLinks.PAYMENT, PaymentLinks.PAYMENT_REL);
        this.cancellationLink = new Link(OrderLinks.CANCEL, OrderLinks.CANCEL_REL);
        this.processor = new OrderResourceProcessor(orderLinks, paymentLinks, invoiceLinks);

        when(invoiceLinks.getInvoiceLink(any(Order.class))).thenReturn(invoiceLink);
        when(paymentLinks.getPaymentLink(any(Order.class))).thenReturn(paymentLink);
        when(orderLinks.getCancelLink(any(Order.class))).thenReturn(cancellationLink);
    }

    @Test
    public void adds_links_to_the_payment_and_cancellation_for_a_new_order() throws Exception {
        Order order = new Order();
        ReflectionTestUtils.setField(order, "status", OrderStatus.PAYMENT_EXPECTED);
        Resource<Order> resource = processor.process(new Resource<>(order));
        // 1
        int expectedSize = 2;
        assertThat(resource.getLinks().size(), is(expectedSize));
        assertTrue(resource.hasLink(PaymentLinks.PAYMENT_REL));
        assertThat(resource.getLink(PaymentLinks.PAYMENT_REL), is(paymentLink));
        assertTrue(resource.hasLink(OrderLinks.CANCEL_REL));
        assertThat(resource.getLink(OrderLinks.CANCEL_REL), is(cancellationLink));
    }

    @Test
    public void does_not_add_a_link_to_the_invoice_for_a_new_order() throws Exception {
        Order order = new Order();
        ReflectionTestUtils.setField(order, "status", OrderStatus.PAYMENT_EXPECTED);
        Resource<Order> resource = processor.process(new Resource<>(order));
        // 1
        int expectedSize = 2;
        assertThat(resource.getLinks().size(), is(expectedSize));
        assertFalse(resource.hasLink(InvoiceLinks.INVOICE_REL));
    }

    @Test
    public void does_not_add_links_to_the_payment_and_cancellation_for_the_paid_order() throws Exception {
        Order order = new Order();
        ReflectionTestUtils.setField(order, "status", OrderStatus.PAID);
        Resource<Order> resource = processor.process(new Resource<>(order));
        // 1
        int expectedSize = 0;
        assertThat(resource.getLinks().size(), is(expectedSize));
        assertFalse(resource.hasLink(PaymentLinks.PAYMENT_REL));
        assertFalse(resource.hasLink(OrderLinks.CANCEL_REL));
    }

    @Test
    public void does_not_add_a_link_to_the_invoice_for_the_paid_order() throws Exception {
        Order order = new Order();
        ReflectionTestUtils.setField(order, "status", OrderStatus.PAID);
        Resource<Order> resource = processor.process(new Resource<>(order));
        // 1
        int expectedSize = 0;
        assertThat(resource.getLinks().size(), is(expectedSize));
        assertFalse(resource.hasLink(InvoiceLinks.INVOICE_REL));
    }

    @Test
    public void does_not_add_links_to_the_payment_and_cancellation_for_shipped_order() throws Exception {
        Order order = new Order();
        ReflectionTestUtils.setField(order, "status", OrderStatus.SHIPPED);
        Resource<Order> resource = processor.process(new Resource<>(order));
        // 1
        int expectedSize = 1;
        assertThat(resource.getLinks().size(), is(expectedSize));
        assertFalse(resource.hasLink(PaymentLinks.PAYMENT_REL));
        assertFalse(resource.hasLink(OrderLinks.CANCEL_REL));
    }

    @Test
    public void does_not_add_links_to_the_payment_and_cancellation_for_delivered_order() throws Exception {
        Order order = new Order();
        ReflectionTestUtils.setField(order, "status", OrderStatus.DELIVERED);
        Resource<Order> resource = processor.process(new Resource<>(order));
        // 1
        int expectedSize = 1;
        assertThat(resource.getLinks().size(), is(expectedSize));
        assertFalse(resource.hasLink(PaymentLinks.PAYMENT_REL));
        assertFalse(resource.hasLink(OrderLinks.CANCEL_REL));
    }

    @Test
    public void adds_a_link_to_the_invoice_of_the_shipped_order() throws Exception {
        Order order = new Order();
        ReflectionTestUtils.setField(order, "status", OrderStatus.SHIPPED);
        Resource<Order> resource = processor.process(new Resource<>(order));
        // 1
        int expectedSize = 1;
        assertThat(resource.getLinks().size(), is(expectedSize));
        assertTrue(resource.hasLink(InvoiceLinks.INVOICE_REL));
        assertThat(resource.getLink(InvoiceLinks.INVOICE_REL), is(invoiceLink));
    }

    @Test
    public void adds_a_link_to_the_invoice_of_the_delivered_order() throws Exception {
        Order order = new Order();
        ReflectionTestUtils.setField(order, "status", OrderStatus.DELIVERED);
        Resource<Order> resource = processor.process(new Resource<>(order));
        // 1
        int expectedSize = 1;
        assertThat(resource.getLinks().size(), is(expectedSize));
        assertTrue(resource.hasLink(InvoiceLinks.INVOICE_REL));
        assertThat(resource.getLink(InvoiceLinks.INVOICE_REL), is(invoiceLink));
    }
}