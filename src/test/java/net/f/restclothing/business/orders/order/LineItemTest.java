package net.f.restclothing.business.orders.order;

import net.f.restclothing.business.orders.product.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @author Artyom Fyodorov
 */
@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class LineItemTest {

    private @Mock Product product;

    @Before
    public void init_product() throws Exception {
        final BigDecimal unitPrice = BigDecimal.valueOf(26.99);
        when(product.getUnitPrice()).thenReturn(unitPrice);
    }

    @Test
    public void calculate_amount() throws Exception {
        LineItem lineItem = LineItem.create(product, 3);

        BigDecimal expectedAmount = BigDecimal.valueOf(80.97);
        BigDecimal actualAmount = lineItem.getAmount();
        assertTrue(expectedAmount.compareTo(actualAmount) == 0);
    }
}