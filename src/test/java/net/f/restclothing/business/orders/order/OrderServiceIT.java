package net.f.restclothing.business.orders.order;

import net.f.restclothing.AbstractIntegrationTest;
import net.f.restclothing.business.TestDataProducer;
import net.f.restclothing.business.customers.customer.Customer;
import net.f.restclothing.business.customers.customer.CustomerRepository;
import net.f.restclothing.business.orders.product.Product;
import net.f.restclothing.business.orders.product.ProductRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithUserDetails;

import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static net.f.restclothing.business.TestDataProducer.CUSTOMER_EMAIL;
import static net.f.restclothing.business.TestDataProducer.getProductStream;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class OrderServiceIT extends AbstractIntegrationTest {

    @Autowired OrderService orderService;
    @Autowired CustomerRepository customerRepository;
    @Autowired OrderRepository orderRepository;
    @Autowired ProductRepository productRepository;
    @Autowired TestDataProducer dataProducer;

    @WithUserDetails(CUSTOMER_EMAIL)
    @Test
    public void successfulOrder() throws Exception {
        // 1
        Iterable<Product> products = productRepository.saveAll(getProductStream().collect(Collectors.toList()));
        Customer customer = dataProducer.getCustomerEntity();
        // 2
        Iterator<Product> productIterator = products.iterator();
        final int expectedItems = 4;
        IntStream.range(0, expectedItems).forEach(ignore -> {
            final int quantity = ThreadLocalRandom.current().nextInt(1, 10);
            orderService.makeOrder(productIterator.next(), quantity, 0, customer.getId());
        });
        // Test
        Page<Order> orders = orderRepository.findByCustomerId(customer.getId(), PageRequest.of(0, 10));
        assertThat(orders, is(not(emptyIterable())));
        Order order = orders.iterator().next();
        assertThat(order.getId(), is(notNullValue()));
        assertEquals(expectedItems, order.numberOfItems());
    }
}