package net.f.restclothing.business.orders.invoice;

import net.f.restclothing.business.orders.order.Order;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Artyom Fyodorov
 */
public class InvoiceTest {

    @Test
    public void mark_paid() throws Exception {
        Invoice invoice = new Invoice();
        ReflectionTestUtils.setField(invoice, "status", InvoiceStatus.CREATED);
        // 1
        assertFalse(invoice.isPaid());
        // 2
        invoice.markPaid();
        assertTrue(invoice.isPaid());
    }

    @Test(expected = IllegalStateException.class)
    public void rejects_double_paid() throws Exception {
        Invoice invoice = new Invoice();
        ReflectionTestUtils.setField(invoice, "status", InvoiceStatus.CREATED);
        // 1
        assertFalse(invoice.isPaid());

        invoice.markPaid();
        invoice.markPaid();
    }

    @Test(expected = IllegalArgumentException.class)
    public void rejects_invoice_with_null_order() throws Exception {
        Order order = null;
        Invoice.create(order);
    }

}