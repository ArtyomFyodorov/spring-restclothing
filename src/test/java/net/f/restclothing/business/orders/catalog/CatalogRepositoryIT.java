package net.f.restclothing.business.orders.catalog;

import net.f.restclothing.AbstractIntegrationTest;
import net.f.restclothing.business.TestDataProducer;
import net.f.restclothing.business.orders.product.Product;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * @author Artyom Fyodorov
 */
public class CatalogRepositoryIT extends AbstractIntegrationTest {

    @Autowired CatalogRepository catalogRepository;

    @Test
    public void catalogTest() throws Exception {
        Set<Product> products = TestDataProducer.getProductStream().collect(Collectors.toSet());
        Catalog product = catalogRepository.save(new Catalog("Testing Catalog", products));
        // 1
        assertNotNull(product.getId());
        assertThat(product.getProducts(), is(not(emptyIterable())));
        assertThat(product.numberOfProducts(), is(products.size()));
        // 2
        assertTrue("The record must be presented",
                catalogRepository.findByName(product.getName()).isPresent());
    }
}