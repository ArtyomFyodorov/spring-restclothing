package net.f.restclothing.business.orders.order;

import net.f.restclothing.business.customers.address.Address;
import net.f.restclothing.business.orders.product.Product;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Artyom Fyodorov
 */
public class OrderTest {

    private Order out;  // order under test

    @Before
    public void initOrder() throws Exception {
        this.out = Order.create(12345L, mock(Address.class));
    }

    @Test
    public void expected_only_one_line_item_in_the_order() throws Exception {
        final LineItem lineItem = mock(LineItem.class);

        out.addLineItem(lineItem);
        out.addLineItem(lineItem);
        out.addLineItem(lineItem);
        out.addLineItem(lineItem);

        assertTrue(out.getLineItems().size() == 1);
    }

    @Test
    public void expected_when_adding_the_same_product_the_quantity_in_a_lineItem_will_be_increased() {
        final Product product = mock(Product.class);

        final int productCount = 4;
        final int productQuantity = 1;
        IntStream.range(0, productCount).forEach(ignore ->
                out.addLineItem(LineItem.create(product, productQuantity)));

        final int expectedQuantity = productQuantity * productCount;
        final int actualQuantity = out.getLineItems().iterator().next().getQuantity();
        assertThat(actualQuantity, is(expectedQuantity));
    }

    @Test
    public void calculate_total() throws Exception {
        IntStream.range(0, 4)
                .mapToObj(ignore -> mock(LineItem.class)) // new mock -> new hashCode
                .forEach(li -> {
                    when(li.getAmount()).thenReturn(BigDecimal.valueOf(42.494));
                    out.addLineItem(li);
                });

        out.calculateTotalAmount();
        BigDecimal expectedAmount = BigDecimal.valueOf(169.98);
        BigDecimal actualAmount = out.getTotalAmount();
        assertTrue(expectedAmount.compareTo(actualAmount) == 0);
    }
}