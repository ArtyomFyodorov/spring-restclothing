package net.f.restclothing.business.orders.invoice;

import net.f.restclothing.AbstractWebIntegrationTest;
import net.f.restclothing.WebMvcTestConfigurator;
import net.f.restclothing.business.RestClothingApplication;
import net.f.restclothing.business.TestDataProducer;
import net.f.restclothing.business.customers.address.Address;
import net.f.restclothing.business.orders.order.Order;
import net.f.restclothing.business.orders.product.Product;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.hateoas.MediaTypes;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Artyom Fyodorov
 */
@Import(WebMvcTestConfigurator.class)
public class InvoiceControllerIT extends AbstractWebIntegrationTest {

    private final String invoiceUrlTemplate = "/orders/{id}/invoice";
    @MockBean InvoiceRepository invoiceRepository;

    @WithMockUser(roles = "CUSTOMER")
    @Test
    public void rejectsANonExistentOrder() throws Exception {
        // id 666 - non-existent order
        mockMvc.perform(get(invoiceUrlTemplate, "666"))
                .andExpect(status().isNotFound());
    }

    @WithMockUser(roles = "CUSTOMER")
    @Test
    public void rejectsAnUnpaidOrder() throws Exception {
        // id 23 - unpaid order
        mockMvc.perform(get(invoiceUrlTemplate, "23"))
                .andExpect(status().isNotFound());
    }

    @WithMockUser(roles = "CUSTOMER")
    @Test
    public void rejectsPaidOrder() throws Exception {
        // id 42 - paid order
        mockMvc.perform(get(invoiceUrlTemplate, "42"))
                .andExpect(status().isNotFound());
    }

    @WithMockUser(roles = "CUSTOMER")
    @Test
    public void showsTheInvoiceOfTheShippedAndDeliveredOrder() throws Exception {
        // id 47 - shipped order
        Order order = TestDataProducer.getOrder(13L, new Address(), new Product(), 1, .0);
        ReflectionTestUtils.setField(order, "id", 47L);
        order.markPaid().markShipped();
        // 2
        Invoice invoice = Invoice.create(order);
        ReflectionTestUtils.setField(invoice, "id", 47L);
        // 3
        given(invoiceRepository.findByOrder(order))
                .willReturn(Optional.of(invoice));
        // Test
        this.mockMvc.perform(get(invoiceUrlTemplate, "47")
                .accept(MediaTypes.HAL_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$._links." + RestClothingApplication.CURIE_NAMESPACE + ":order.href", notNullValue()));
    }

    @WithMockUser(roles = "CUSTOMER")
    @Test
    public void showsTheInvoiceOfTheDeliveredOrder() throws Exception {
        // id 69 - delivered order
        Order order = TestDataProducer.getOrder(13L, new Address(), new Product(), 1, .0);
        ReflectionTestUtils.setField(order, "id", 69L);
        order.markPaid().markShipped().markDelivered();
        // 2
        Invoice invoice = Invoice.create(order);
        ReflectionTestUtils.setField(invoice, "id", 69L);
        // 3
        given(invoiceRepository.findByOrder(order))
                .willReturn(Optional.of(invoice));
        // Test
        this.mockMvc.perform(get(invoiceUrlTemplate, "69")
                .accept(MediaTypes.HAL_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$._links." + RestClothingApplication.CURIE_NAMESPACE + ":order.href", notNullValue()));
    }

}