package net.f.restclothing.business.orders.product;

import net.f.restclothing.AbstractIntegrationTest;
import net.f.restclothing.business.TestDataProducer;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class ProductRepositoryIT extends AbstractIntegrationTest {

    private @Autowired ProductRepository productRepository;

    @Test
    public void productsTest() throws Exception {
        // Preparation
        Set<Product> productSet = TestDataProducer.getProductStream().collect(Collectors.toSet());
        Iterable<Product> products = productRepository.saveAll(productSet);
        Product product = products.iterator().next();
        // Test
        final String expectedName = product.getName();
        final String expectedSku = product.getSku();
        final BigDecimal expectedPrice = product.getUnitPrice();
        final Optional<Product> maybeProduct = productRepository.findById(product.getId());

        assertThat(maybeProduct).hasValueSatisfying(p -> {
            assertThat(p.getName()).isEqualTo(expectedName);
            assertThat(p.getSku()).isEqualTo(expectedSku);
            assertThat(p.getUnitPrice()).isEqualByComparingTo(expectedPrice);
        });

    }
}