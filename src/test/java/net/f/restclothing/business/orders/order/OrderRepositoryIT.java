package net.f.restclothing.business.orders.order;

import net.f.restclothing.AbstractIntegrationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.test.context.support.WithUserDetails;

import static net.f.restclothing.business.TestDataProducer.ADMIN_EMAIL;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class OrderRepositoryIT extends AbstractIntegrationTest {

    @Autowired OrderRepository orderRepository;

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void findByIdThrowsAnException_WithoutAnyRole() throws Exception {
        orderRepository.findById(12345L);
    }

    @WithUserDetails(ADMIN_EMAIL)
    @Test
    public void successfulFindById_WithCustomerAndAdminRoles() throws Exception {
        assertFalse(orderRepository.findById(12345L).isPresent());
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void findAllThrowsAnException_WithoutAnyRole() throws Exception {
        orderRepository.findAll(PageRequest.of(0, 10));
    }

    @WithUserDetails(ADMIN_EMAIL)
    @Test
    public void successfulFindsAll_WithCustomerAndAdminRoles() throws Exception {
        Page<Order> orders = orderRepository.findAll(PageRequest.of(0, 10));
        assertThat(orders, is(emptyIterable()));
    }

}