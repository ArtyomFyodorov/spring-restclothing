package net.f.restclothing.business.orders.product;

import net.f.restclothing.AbstractWebIntegrationTest;
import net.f.restclothing.business.RestClothingApplication;
import net.f.restclothing.business.TestDataProducer;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static net.f.restclothing.business.TestDataProducer.CUSTOMER_EMAIL;
import static net.f.restclothing.business.TestDataProducer.CUSTOMER_PASSWORD;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.hateoas.MediaTypes.HAL_JSON;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Artyom Fyodorov
 */
public class ProductControllerIT extends AbstractWebIntegrationTest {

    private final String productUrlTemplate = "/products/{id}/order";
    @Autowired TestDataProducer dataProducer;

    @Test
    public void rejectsUnauthorizedRequest() throws Exception {
        this.mockMvc.perform(
                post(productUrlTemplate, 42L))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void rejectsBecauseTheProductIsNotFound() throws Exception {
        this.mockMvc.perform(
                post(productUrlTemplate, 42L)
                        .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void responseReturnsALinkToTheOrder() throws Exception {
        Product product = dataProducer.getProductEntity();
        // Test
        this.mockMvc.perform(
                get("/products/{id}", product.getId())
        )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(HAL_JSON))
                .andExpect(jsonPath("$._links." + RestClothingApplication.CURIE_NAMESPACE + ":product-order.href", notNullValue()));
    }

}
