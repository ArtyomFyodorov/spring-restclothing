package net.f.restclothing.business.orders.invoice;

import net.f.restclothing.AbstractIntegrationTest;
import net.f.restclothing.business.TestDataProducer;
import net.f.restclothing.business.customers.address.Address;
import net.f.restclothing.business.customers.address.AddressRepository;
import net.f.restclothing.business.customers.address.Country;
import net.f.restclothing.business.orders.order.Order;
import net.f.restclothing.business.orders.order.OrderRepository;
import net.f.restclothing.business.orders.product.Product;
import net.f.restclothing.business.orders.product.ProductRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * @author Artyom Fyodorov
 */
public class InvoiceRepositoryIT extends AbstractIntegrationTest {

    @Autowired ProductRepository productRepository;
    @Autowired InvoiceRepository invoiceRepository;
    @Autowired OrderRepository orderRepository;
    @Autowired AddressRepository addressRepository;

    @Test
    public void invoiceTest() throws Exception {
        // 1
        Product product = TestDataProducer.getProduct();
        productRepository.save(product);
        // 2
        Address shippingAddress = new Address("2650 Wisconsin Avenue, N.W.",
                "Washington", "DC", Country.UNITED_STATES, 20007);
        addressRepository.save(shippingAddress);
        // 3
        Long customerNumber = 12345L;
        Order order = TestDataProducer.getOrder(customerNumber, shippingAddress, product, 1, .07);
        Order wellKnownOrder = orderRepository.save(order);
        // Test 1
        Invoice invoice = invoiceRepository.save(Invoice.create(wellKnownOrder));
        assertNotNull(invoice.getId());
        assertThat(invoice.getCustomerId(), is(wellKnownOrder.getCustomerId()));
        // Test 2
        assertTrue(invoiceRepository.findByOrder(order).isPresent());
    }
}