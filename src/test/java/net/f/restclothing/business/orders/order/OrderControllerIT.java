package net.f.restclothing.business.orders.order;

import net.f.restclothing.AbstractWebIntegrationTest;
import net.f.restclothing.business.TestDataProducer;
import net.f.restclothing.business.customers.customer.Customer;
import net.f.restclothing.business.orders.product.Product;
import net.f.restclothing.business.orders.product.ProductRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;

import static net.f.restclothing.business.TestDataProducer.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Artyom Fyodorov
 */
public class OrderControllerIT extends AbstractWebIntegrationTest {

    private final String orderCancellationUrlTemplate = "/orders/{id}" + OrderLinks.CANCEL;
    @Autowired OrderRepository orderRepository;
    @Autowired ProductRepository productRepository;
    @Autowired TestDataProducer dataProducer;

    @Test
    public void rejectsUnauthorizedRequest() throws Exception {
        mockMvc.perform(delete(orderCancellationUrlTemplate, 12345L))
                .andExpect(status().isUnauthorized());
    }

    @WithUserDetails(ADMIN_EMAIL)
    @Test
    public void rejectsANonExistentOrder() throws Exception {
        mockMvc.perform(delete(orderCancellationUrlTemplate, 12345L))
                .andExpect(status().isNotFound());
    }

    @WithUserDetails(ADMIN_EMAIL)
    @Test
    public void rejectsAnOrderBelongingToAnotherCustomer() throws Exception {
        // 1
        Customer customer = dataProducer.getCustomerEntity();
        // 2
        Product product = productRepository.save(getProduct());
        Order order = orderRepository.save(
                getOrder(customer.getId(), customer.getAddresses().iterator().next(), product, 1, .0));
        // Test
        mockMvc.perform(delete(orderCancellationUrlTemplate, order.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void rejectsPaidOrder() throws Exception {
        // 1
        Customer customer = dataProducer.getCustomerEntity();
        // 2
        Product product = productRepository.save(getProduct());
        Order order = getOrder(customer.getId(), customer.getAddresses().iterator().next(), product, 1, .0);
        order.markPaid();
        orderRepository.save(order);
        // Test
        mockMvc.perform(delete(orderCancellationUrlTemplate, order.getId())
                .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void successfulCancellation() throws Exception {
        // 1
        Customer customer = dataProducer.getCustomerEntity();
        // 2
        Product product = productRepository.save(getProduct());
        Order order = orderRepository.save(
                getOrder(customer.getId(), customer.getAddresses().iterator().next(), product, 1, .0));
        // Test 1
        this.mockMvc.perform(delete(orderCancellationUrlTemplate, order.getId())
                .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isNoContent());
        // Test 2
        this.mockMvc.perform(get("/orders/{id}", order.getId())
                .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isNotFound());
    }

}