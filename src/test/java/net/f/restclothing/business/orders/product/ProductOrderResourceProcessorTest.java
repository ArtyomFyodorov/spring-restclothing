package net.f.restclothing.business.orders.product;

import net.f.restclothing.business.TestDataProducer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author Artyom Fyodorov
 */
@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class ProductOrderResourceProcessorTest {

    private @Mock ProductLinks productLinks;

    private Link orderLink;
    private ProductOrderResourceProcessor processor;

    @Before
    public void set_up() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        ServletRequestAttributes requestAttributes = new ServletRequestAttributes(request);
        RequestContextHolder.setRequestAttributes(requestAttributes);

        this.orderLink = new Link(ProductLinks.ORDER, ProductLinks.ORDER_REL);
        this.processor = new ProductOrderResourceProcessor(productLinks);

        when(productLinks.getOrderLink(any(Product.class))).thenReturn(orderLink);
    }

    @Test
    public void adds_the_order_link_for_the_current_customer() {
        final Product product = TestDataProducer.getProduct();
        Resource<Product> resource = processor.process(new Resource<>(product));
        int expectedSize = 1;
        assertEquals(expectedSize, resource.getLinks().size());
        assertTrue(resource.hasLink(ProductLinks.ORDER_REL));
        assertThat(resource.getLink(ProductLinks.ORDER_REL), is(orderLink));
    }
}