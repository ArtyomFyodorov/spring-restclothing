package net.f.restclothing.business;

import net.f.restclothing.AbstractWebIntegrationTest;
import net.f.restclothing.business.customers.customer.Customer;
import net.f.restclothing.business.orders.catalog.Catalog;
import net.f.restclothing.business.orders.catalog.CatalogRepository;
import net.f.restclothing.business.orders.invoice.Invoice;
import net.f.restclothing.business.orders.invoice.InvoiceRepository;
import net.f.restclothing.business.orders.order.Order;
import net.f.restclothing.business.orders.order.OrderRepository;
import net.f.restclothing.business.orders.product.Product;
import net.f.restclothing.business.orders.product.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.hypermedia.LinksSnippet;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;

import javax.servlet.RequestDispatcher;
import java.util.Collections;

import static net.f.restclothing.business.RestClothingApplication.CURIE_NAMESPACE;
import static net.f.restclothing.business.TestDataProducer.CUSTOMER_EMAIL;
import static net.f.restclothing.business.TestDataProducer.CUSTOMER_PASSWORD;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.hateoas.MediaTypes.HAL_JSON;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.JsonFieldType.STRING;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Artyom Fyodorov
 */
public class RestClothingApiDocumentationTest extends AbstractWebIntegrationTest {

    private final String ordersPath = "/orders";
    private final String orderPath = "/orders/{id}";
    private final String orderPaymentPath = "/orders/{id}/payment";
    private final String orderInvoicePath = "/orders/{id}/invoice";
    private final String catalogsPath = "/catalogs";
    private final String catalogPath = "/catalogs/{id}";
    private final String productsPath = "/products";
    private final String productPath = "/products/{id}";
    private final String productOrderPath = "/products/{id}/order";
    private final LinksSnippet canonicalOrderLinks = links(
            linkWithRel("curies").description("CUR-ies."),
            linkWithRel("self").description("Canonical link for this <<resources-order,order>>."),
            linkWithRel(CURIE_NAMESPACE + ":order").description("This <<resources-order,order>>."));

    @Autowired ProductRepository productRepository;
    @Autowired OrderRepository orderRepository;
    @Autowired CatalogRepository catalogRepository;
    @Autowired InvoiceRepository invoiceRepository;
    @Autowired TestDataProducer dataProducer;

    private Order wellKnownOrder;
    private Product wellKnownProduct;
    private Catalog wellKnownCatalog;

    @Before
    public void init() throws Exception {
        Customer wellKnownCustomer = dataProducer.getCustomerEntity();

        this.wellKnownProduct = productRepository.save(TestDataProducer.getProduct());
        this.wellKnownCatalog = catalogRepository.save(new Catalog("Testing Catalog", Collections.singleton(wellKnownProduct)));
        this.wellKnownOrder = orderRepository.save(TestDataProducer.getOrder(wellKnownCustomer.getId(),
                wellKnownCustomer.getAddresses().get(0), wellKnownProduct, 1, .18));
    }

    @Test
    public void errorExample() throws Exception {
        this.mockMvc
                .perform(get("/error")
                        .requestAttr(RequestDispatcher.ERROR_STATUS_CODE, 400)
                        .requestAttr(RequestDispatcher.ERROR_REQUEST_URI, "/products/1/order/quantity/5/address/10")
                        .requestAttr(RequestDispatcher.ERROR_MESSAGE,
                                "Can't find address by index 10"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("error", is("Bad Request")))
                .andExpect(jsonPath("timestamp", is(notNullValue())))
                .andExpect(jsonPath("status", is(400)))
                .andExpect(jsonPath("path", is(notNullValue())))
                .andDo(document("error-example",
                        responseFields(
                                fieldWithPath("error").description("The HTTP error that occurred, e.g. `Bad Request`"),
                                fieldWithPath("message").description("A description of the cause of the error"),
                                fieldWithPath("path").description("The path to which the request was made"),
                                fieldWithPath("status").description("The HTTP status code, e.g. `400`"),
                                fieldWithPath("timestamp").description("The time, in milliseconds, at which the error occurred"))));
    }

    @Test
    public void index() throws Exception {
        this.mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andDo(document("index-example",
                        curiesLink.and(
                                linkWithRel(CURIE_NAMESPACE + ":customers").description("The <<resources-customers,Customers resource>>."),
                                linkWithRel(CURIE_NAMESPACE + ":orders").description("The <<resources-orders,Orders resource>>."),
                                linkWithRel(CURIE_NAMESPACE + ":products").description("The <<resources-products,Products resource>>."),
                                linkWithRel(CURIE_NAMESPACE + ":catalogs").description("The <<resources-catalogs,Catalogs resource>>."),
                                linkWithRel("profile").description("The http://alps.io/[ALPS] profile for the service.")),
                        responseFields(
                                subsectionWithPath("_links").description("<<resources-index-links,Links>> to other resources."))));

    }

    @Test
    public void productsList() throws Exception {
        // Preparation
        invoiceRepository.deleteAll();
        orderRepository.deleteAll();
        catalogRepository.deleteAll();
        productRepository.deleteAll();
        // Test
        this.mockMvc.perform(get(productsPath))
                .andExpect(status().isOk())
                .andDo(document("products-list",
                        curiesLink.and(
                                linkWithRel("self").description("Canonical link for these <<resources-products,products>>."),
                                linkWithRel("profile").description("The http://alps.io/[ALPS] profile for the service.")
                        ),
                        responseFields(
                                subsectionWithPath("_embedded." + CURIE_NAMESPACE + ":products").description("An array of <<resources-product,Product resources>>."),
                                subsectionWithPath("_links").description("<<resources-products-list-links, Links>> to other resources."),
                                subsectionWithPath("page").description("Additional page metadata."))
                        )
                );
    }

    @Test
    public void getProductById() throws Exception {
        this.mockMvc.perform(get(productPath, wellKnownProduct.getId()))
                .andExpect(status().isOk())
                .andDo(document("product-get",
                        curiesLink.and(
                                linkWithRel("self").description("Canonical link for this <<resources-product,product>>."),
                                linkWithRel(CURIE_NAMESPACE + ":product").description("This <<resources-product,product>>."),
                                linkWithRel(CURIE_NAMESPACE + ":product-order").description("<<resources-product-order,Order>> link for this <<resources-product,product>>.")
                        ),
                        responseFields(
                                fieldWithPath("name").description("The name of the product."),
                                fieldWithPath("sku").description("The SKU of the product."),
                                fieldWithPath("unitPrice").description("The price of the product."),
                                subsectionWithPath("_links").description("<<resources-product-links,Links>> to other resources.")
                        )
                ));
    }

    @Test
    public void makeOrder() throws Exception {
        this.mockMvc.perform(
                post(productOrderPath, wellKnownProduct.getId())
                        .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isNoContent())
                .andDo(document("product-order-create"));
    }

    @Test
    public void makeOrderWithPathParams() throws Exception {
        this.mockMvc.perform(
                RestDocumentationRequestBuilders.post(productOrderPath + "/quantity/{q}/address/{a}", wellKnownProduct.getId(), 5, 0)
                        .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isNoContent())
                .andDo(document("product-order-params-create", pathParameters(
                        parameterWithName("id").description("The product unique identifier."),
                        parameterWithName("q").description("The quantity of the product."),
                        parameterWithName("a").description("The index of the shipping address in the address list (zero-based indexing).")
                )));
    }

    @Test
    public void paymentOrder() throws Exception {
        this.mockMvc.perform(put(orderPaymentPath, wellKnownOrder.getId())
                .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(HAL_JSON))
                .andDo(document("order-payment-update",
                        curiesLink.and(
                                linkWithRel(CURIE_NAMESPACE + ":order").description("<<resources-order, Link>> to the order resource.")
                        ),
                        responseFields(
                                fieldWithPath("totalAmount").description("The total amount of payment."),
                                fieldWithPath("creditCard.cardNumber").type(STRING).description("The credit card number."),
                                subsectionWithPath("_links").description("<<resources-order-payment-links,Links>> to other resources.")
                        )
                ));
    }

    @Test
    public void getInvoice() throws Exception {
        // Preparation
        Order updatedOrder = orderRepository.save(wellKnownOrder.markPaid().markShipped().markDelivered());
        invoiceRepository.save(Invoice.create(wellKnownOrder).markPaid());
        // Test
        this.mockMvc.perform(get(orderInvoicePath, updatedOrder.getId())
                .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(HAL_JSON))
                .andDo(document("order-invoice-get",
                        curiesLink.and(
                                linkWithRel(CURIE_NAMESPACE + ":order").description("<<resources-order, Link>> to the order resource.")
                        ),
                        responseFields(
                                fieldWithPath("customerId").description("The customer number who made the order."),
                                fieldWithPath("status").description("The current status of the invoice."),
                                fieldWithPath("amountOfInvoice").description("The amount of the invoice."),
                                subsectionWithPath("_links").description("<<resources-order-invoice-links,Links>> to other resources.")
                        )
                ));
    }

    @Test
    public void ordersList() throws Exception {
        // Preparation
        orderRepository.deleteAll();
        // Test
        this.mockMvc.perform(get(ordersPath)
                .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(HAL_JSON))
                .andDo(document("orders-list",
                        curiesLink.and(
                                linkWithRel("self").description("Canonical link for these <<resources-products,products>>."),
                                linkWithRel("profile").description("The http://alps.io/[ALPS] profile for the service."),
                                linkWithRel("search").description("The search resource returns links for all query methods.")
                        ),
                        responseFields(
                                subsectionWithPath("_embedded." + CURIE_NAMESPACE + ":orders").description("An array of <<resources-order,Order resources>>."),
                                subsectionWithPath("_links").description("<<resources-orders-list-links, Links>> to other resources."),
                                subsectionWithPath("page").description("Additional page metadata."))
                        )
                );
    }

    @Test
    public void getUnpaidOrder() throws Exception {
        this.mockMvc.perform(get(orderPath, wellKnownOrder.getId())
                .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(HAL_JSON))
                .andDo(document("order-unpaid-get",
                        canonicalOrderLinks.and(
                                linkWithRel(CURIE_NAMESPACE + ":cancel").description("Link to cancel this <<resources-order,order>>."),
                                linkWithRel(CURIE_NAMESPACE + ":payment").description("Link to <<resources-order-payment-update,pay>> for this <<resources-order,order>>.")
                        ),
                        responseFields(
                                fieldWithPath("customerId").description("The customer number who made the order."),
                                fieldWithPath("status").description("The current status of the order."),
                                fieldWithPath("totalAmount").description("The total amount of the order."),
                                fieldWithPath("shippingAddress.street").description("The part of the shipping address."),
                                fieldWithPath("shippingAddress.city").description("The part of the shipping address."),
                                fieldWithPath("shippingAddress.state").description("The part of the shipping address."),
                                fieldWithPath("shippingAddress.country").description("The part of the shipping address."),
                                fieldWithPath("shippingAddress.zipCode").description("The part of the shipping address."),
                                fieldWithPath("lineItems[]").description("An array of line items."),
                                fieldWithPath("lineItems[].quantity").description("The quantity of the product."),
                                fieldWithPath("lineItems[].unitPrice").description("The unit price of the product."),
                                fieldWithPath("lineItems[].amount").description("The amount of the line item."),
                                subsectionWithPath("lineItems[]._links." + CURIE_NAMESPACE + ":product").description("<<resources-product, Link>> to product resource."),
                                subsectionWithPath("_links").description("<<resources-order-links,Links>> to other resources.")
                        )
                ));
    }

    @Test
    public void getAPaidOrder() throws Exception {
        // Preparation
        Order updatedOrder = orderRepository.save(wellKnownOrder.markPaid());
        // Test
        this.mockMvc.perform(get(orderPath, updatedOrder.getId())
                .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(HAL_JSON))
                .andDo(document("order-paid-get", canonicalOrderLinks));
    }

    @Test
    public void getShippedOrder() throws Exception {
        // Preparation
        Order updatedOrder = orderRepository.save(wellKnownOrder.markPaid().markShipped());
        // Test
        this.mockMvc.perform(get(orderPath, updatedOrder.getId())
                .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(HAL_JSON))
                .andDo(document("order-shipped-get",
                        canonicalOrderLinks.and(
                                linkWithRel(CURIE_NAMESPACE + ":invoice").description("Link to the <<resources-order-invoice-retrieve,invoice>> of this <<resources-order,order>>.")
                        )
                ));
    }

    @Test
    public void catalogsList() throws Exception {
        // Preparation
        catalogRepository.deleteAll();
        // Test
        this.mockMvc.perform(get(catalogsPath))
                .andExpect(status().isOk())
                .andDo(document("catalogs-list",
                        curiesLink.and(
                                linkWithRel("self").description("Canonical link for these <<resources-catalogs,catalogs>>."),
                                linkWithRel("profile").description("The http://alps.io/[ALPS] profile for the service.")
                        ),
                        responseFields(
                                subsectionWithPath("_embedded." + CURIE_NAMESPACE + ":catalogs").description("An array of <<resources-catalog,Catalog resources>>."),
                                subsectionWithPath("_links").description("<<resources-catalogs-list-links, Links>> to other resources.")
                        )
                ));
    }

    @Test
    public void getCatalogById() throws Exception {
        this.mockMvc.perform(get(catalogPath, wellKnownCatalog.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(HAL_JSON))
                .andDo(document("catalog-get",
                        curiesLink.and(
                                linkWithRel("self").description("Canonical link for this <<resources-catalog,catalog>>."),
                                linkWithRel(CURIE_NAMESPACE + ":catalog").description("This <<resources-catalog,catalog>>."),
                                linkWithRel(CURIE_NAMESPACE + ":products").description("The <<resources-products,Products resource>> for this <<resources-catalog,catalog>>.")
                        ),
                        responseFields(
                                fieldWithPath("name").description("The name of the catalog."),
                                subsectionWithPath("_links").description("<<resources-catalog-links,Links>> to other resources.")
                        )
                ));
    }
}