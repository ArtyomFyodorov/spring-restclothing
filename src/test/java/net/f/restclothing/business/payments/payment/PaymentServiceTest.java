package net.f.restclothing.business.payments.payment;

import net.f.restclothing.business.customers.address.Address;
import net.f.restclothing.business.customers.creditcard.exceptions.PaymentException;
import net.f.restclothing.business.customers.customer.Customer;
import net.f.restclothing.business.customers.customer.CustomerRepository;
import net.f.restclothing.business.orders.invoice.InvoiceRepository;
import net.f.restclothing.business.orders.order.Order;
import net.f.restclothing.business.orders.order.OrderRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


/**
 * @author Artyom Fyodorov
 */
@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class PaymentServiceTest {

    public @Rule ExpectedException exception = ExpectedException.none();
    private PaymentService paymentService;
    private @Mock CustomerRepository customerRepository;
    private @Mock PaymentRepository paymentRepository;
    private @Mock InvoiceRepository invoiceRepository;
    private @Mock OrderRepository orderRepository;
    private @Mock Order order;

    @Before
    public void set_up() throws Exception {
        this.paymentService = new PaymentServiceImpl(customerRepository,
                orderRepository, invoiceRepository, paymentRepository);

    }

    @Test(expected = PaymentException.class)
    public void rejects_already_paid_order() throws Exception {

        Order orderStub = Order.create(12345L, new Address());
        ReflectionTestUtils.setField(orderStub, "id", 42L);
        orderStub.markPaid();

        this.paymentService.confirmAndPay(orderStub, "mock@test.com");

    }

    @Test(expected = UsernameNotFoundException.class)
    public void rejects_payment_if_customer_is_not_found() throws Exception {
        when(customerRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        this.paymentService.confirmAndPay(order, "");  // principal name is empty
    }

    @Test(expected = PaymentException.class)
    public void rejects_payment_if_no_credit_card_found() throws Exception {
        Customer customerStub = new Customer("", "", "mock@test.com", "");
        when(customerRepository.findByEmail(customerStub.getEmail()))
                .thenReturn(Optional.of(customerStub));

        this.paymentService.confirmAndPay(order, customerStub.getEmail());

    }
}