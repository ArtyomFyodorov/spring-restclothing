package net.f.restclothing.business.payments.payment;

import net.f.restclothing.AbstractWebIntegrationTest;
import net.f.restclothing.WebMvcTestConfigurator;
import net.f.restclothing.business.RestClothingApplication;
import org.junit.Test;
import org.springframework.context.annotation.Import;
import org.springframework.hateoas.MediaTypes;
import org.springframework.security.test.context.support.WithUserDetails;

import static net.f.restclothing.WebMvcTestConfigurator.*;
import static net.f.restclothing.business.TestDataProducer.*;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Artyom Fyodorov
 */
@Import(WebMvcTestConfigurator.class)
public class PaymentControllerIT extends AbstractWebIntegrationTest {

    private final String orderUrlTemplate = "/orders/{id}" + PaymentLinks.PAYMENT;

    @Test
    public void rejectsUnauthorizedRequest() throws Exception {
        mockMvc.perform(put(orderUrlTemplate, NON_EXISTENT_ORDER))
                .andExpect(status().isUnauthorized());
    }

    @WithUserDetails(ADMIN_EMAIL)
    @Test
    public void rejectsANonExistentOrder() throws Exception {
        mockMvc.perform(put(orderUrlTemplate, NON_EXISTENT_ORDER))
                .andExpect(status().isNotFound());
    }

    @WithUserDetails(ADMIN_EMAIL)
    @Test
    public void rejectsAlreadyPaidOrder() throws Exception {
        mockMvc.perform(put(orderUrlTemplate, PAID_ORDER))
                .andExpect(status().isNotFound());
    }

    @Test
    public void successfulPayment() throws Exception {
        this.mockMvc.perform(put(orderUrlTemplate, PAYMENT_EXPECTED)
                .with(httpBasic(CUSTOMER_EMAIL, CUSTOMER_PASSWORD))
        ).andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaTypes.HAL_JSON))
                .andExpect(jsonPath("$._links." + RestClothingApplication.CURIE_NAMESPACE + ":order.href", notNullValue()));
    }
}