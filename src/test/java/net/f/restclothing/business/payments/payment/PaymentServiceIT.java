package net.f.restclothing.business.payments.payment;

import net.f.restclothing.AbstractIntegrationTest;
import net.f.restclothing.business.TestDataProducer;
import net.f.restclothing.business.customers.address.Address;
import net.f.restclothing.business.customers.customer.Customer;
import net.f.restclothing.business.orders.invoice.Invoice;
import net.f.restclothing.business.orders.invoice.InvoiceRepository;
import net.f.restclothing.business.orders.invoice.InvoiceStatus;
import net.f.restclothing.business.orders.order.Order;
import net.f.restclothing.business.orders.order.OrderRepository;
import net.f.restclothing.business.orders.order.OrderStatus;
import net.f.restclothing.business.orders.product.Product;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;

import java.util.Optional;

import static net.f.restclothing.business.TestDataProducer.CUSTOMER_EMAIL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

/**
 * @author Artyom Fyodorov
 */
public class PaymentServiceIT extends AbstractIntegrationTest {

    @Autowired OrderRepository orderRepository;
    @Autowired InvoiceRepository invoiceRepository;
    @Autowired PaymentService paymentService;
    @Autowired PaymentRepository paymentRepository;
    @Autowired TestDataProducer dataProducer;

    @WithUserDetails(CUSTOMER_EMAIL)
    @Test
    public void markOrderAndInvoiceAsPaid() {
        // 1
        final Customer customer = dataProducer.getCustomerEntity();
        final Product product = dataProducer.getProductEntity();
        // 2
        Long customerNumber = customer.getId();
        Address shippingAddress = customer.getAddresses().iterator().next();
        Order order = TestDataProducer.getOrder(customerNumber, shippingAddress, product, 1, .07);
        // Test 1
        final Order savedOrder = orderRepository.save(order);
        assertEquals(savedOrder.getStatus(), OrderStatus.PAYMENT_EXPECTED);
        // Test 2
        String email = customer.getEmail();
        Payment payment = paymentService.confirmAndPay(savedOrder, email);
        assertNotNull(payment.getId());
        // Test 3
        Optional<Invoice> invoiceMaybe = invoiceRepository.findByOrder(order);
        assertTrue(invoiceMaybe.isPresent());
        Invoice invoice = invoiceMaybe.get();
        assertEquals(invoice.getStatus(), InvoiceStatus.PAID);
        // Test 4
        Optional<Order> maybeOrder = orderRepository.findById(savedOrder.getId());
        assertThat(maybeOrder)
                .hasValueSatisfying(o -> assertThat(o.getStatus()).isEqualTo(OrderStatus.PAID));
        // Test 5
        final Optional<Payment> maybePayment = paymentRepository.findById(payment.getId());
        assertThat(maybePayment)
                .hasValueSatisfying(p -> {
                    assertThat(p.getTotalAmount()).isEqualTo(invoice.getAmountOfInvoice());
                    assertThat(p.getCreditCard()).isEqualTo(customer.getCreditCard());
                });
    }
}