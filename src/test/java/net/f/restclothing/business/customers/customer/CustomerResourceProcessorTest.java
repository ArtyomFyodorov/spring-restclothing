package net.f.restclothing.business.customers.customer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static net.f.restclothing.business.TestDataProducer.getCustomer;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author Artyom Fyodorov
 */
@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class CustomerResourceProcessorTest {

    private @Mock CustomerLinks customerLinks;

    private Link updateLink;
    private Link ordersLink;
    private CustomerResourceProcessor processor;

    @Before
    public void set_up() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        ServletRequestAttributes requestAttributes = new ServletRequestAttributes(request);
        RequestContextHolder.setRequestAttributes(requestAttributes);

        this.updateLink = new Link("/", CustomerLinks.UPDATE_REL);
        this.ordersLink = new Link("/", CustomerLinks.ORDERS_REL);
        this.processor = new CustomerResourceProcessor(customerLinks);

        when(customerLinks.getUpdateLink(any(Customer.class))).thenReturn(updateLink);
        when(customerLinks.getOrdersLink()).thenReturn(ordersLink);
    }

    @Test
    public void adds_update_and_orders_links_for_the_current_customer() {
        Resource<Customer> resource = processor.process(new Resource<>(getCustomer()));
        int expectedSize = 2;
        assertThat(resource.getLinks().size(), is(expectedSize));
        assertTrue(resource.hasLink(CustomerLinks.UPDATE_REL));
        assertThat(resource.getLink(CustomerLinks.UPDATE_REL), is(updateLink));
        assertTrue(resource.hasLink(CustomerLinks.ORDERS_REL));
        assertThat(resource.getLink(CustomerLinks.ORDERS_REL), is(ordersLink));
    }
}