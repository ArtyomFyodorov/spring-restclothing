package net.f.restclothing.business.customers.customer;

import net.f.restclothing.AbstractIntegrationTest;
import net.f.restclothing.business.TestDataProducer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;

import static net.f.restclothing.business.TestDataProducer.CUSTOMER_EMAIL;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * @author Artyom Fyodorov
 */
public class CustomerRepositoryIT extends AbstractIntegrationTest {

    @Autowired CustomerRepository customerRepository;
    @Autowired PasswordEncoder passwordEncoder;
    @Autowired TestDataProducer testDataProducer;
    private Customer cut; // customer under test

    @Before
    public void setUp() throws Exception {
        this.cut = testDataProducer.getCustomerEntity();
    }

    @Test
    public void shouldEncodedPassword() {
        // given
        Customer customer = TestDataProducer.getCustomer();
        final String rawPassword = "password";
        customer.setPassword(rawPassword);
        // when
        this.cut = customerRepository.save(customer);
        // then
        assertThat(customer.getId(), is(notNullValue()));
        assertTrue(passwordEncoder.matches(rawPassword, customer.getPassword()));
    }

    @Test
    public void successfulFindByEmail() throws Exception {
        assertNotNull(customerRepository.findByEmail(cut.getEmail()));
    }

    @WithMockUser(roles = "ADMIN")
    @Test
    public void successfullyFindsCustomer_RoleAdmin() {
        assertTrue(customerRepository.findById(cut.getId()).isPresent());
    }

    @WithMockUser(CUSTOMER_EMAIL)
    @Test
    public void successfullyFindsCustomer_RoleCustomer() {
        assertTrue(customerRepository.findByEmail(CUSTOMER_EMAIL).isPresent());
    }

    @WithMockUser(CUSTOMER_EMAIL)
    @Test(expected = AccessDeniedException.class)
    public void rejectsUsernameDoNotMatch_RoleCustomer() {
        Customer secondCustomer = TestDataProducer.getCustomer();
        secondCustomer.setEmail("john.smith@mail.com");
        // Test 1
        secondCustomer = customerRepository.save(secondCustomer);
        assertThat(secondCustomer.getId(), is(notNullValue()));
        // Test 2
        customerRepository.findById(secondCustomer.getId());
    }

    @WithMockUser(CUSTOMER_EMAIL)
    @Test
    public void successfullyFindsCurrentAuthCustomer() throws Exception {
        Customer actualCustomer = customerRepository.findMyself();
        assertEquals(actualCustomer.getEmail(), CUSTOMER_EMAIL);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void rejectsRequestInNotAuthorized() throws Exception {
        customerRepository.findMyself();
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void findAll_WithoutRole() {
        customerRepository.findAll(PageRequest.of(0, 10));
    }

    @WithMockUser(roles = "ADMIN")
    @Test
    public void findAll_RoleAdmin() throws Exception {
        Page<Customer> customers = customerRepository.findAll(PageRequest.of(0, 10));
        assertThat(customers, is(not(emptyIterable())));
    }

    @WithMockUser(roles = "CUSTOMER")
    @Test(expected = AccessDeniedException.class)
    public void findAll_RoleCustomer() throws Exception {
        customerRepository.findAll(PageRequest.of(0, 10));
    }

}