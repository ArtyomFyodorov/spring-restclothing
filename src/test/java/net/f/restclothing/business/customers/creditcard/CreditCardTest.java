package net.f.restclothing.business.customers.creditcard;

import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class CreditCardTest {

    private static final String CARD_NUMBER = "1029-----3847 5610  --- -  2938";
    private static final int CURRENT_YEAR_PLUS_ONE = Year.now().plusYears(1).getValue();

    @Test
    public void check_format_credit_card_number() throws Exception {
        CreditCard creditCard = CreditCard.create(CARD_NUMBER, "Duke Mascot",
                Month.JANUARY, Year.of(CURRENT_YEAR_PLUS_ONE), CreditCardType.VISA);
        String expected = "1029384756102938";
        assertThat(creditCard.getCardNumber(), is(expected));
    }

    @Test
    public void discovers_expired_credit_card() throws Exception {
        CreditCard creditCard = CreditCard.create(CARD_NUMBER, "Duke Mascot",
                Month.JANUARY, Year.of(CURRENT_YEAR_PLUS_ONE), CreditCardType.VISA);

        assertThat(creditCard.isValid(), is(TRUE));
        assertThat(creditCard.isValid(LocalDate.of(CURRENT_YEAR_PLUS_ONE, 1, 1)), is(FALSE));
    }
}