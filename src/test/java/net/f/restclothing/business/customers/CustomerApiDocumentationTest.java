package net.f.restclothing.business.customers;

import net.f.restclothing.AbstractWebIntegrationTest;
import net.f.restclothing.business.TestDataProducer;
import net.f.restclothing.business.customers.address.Address;
import net.f.restclothing.business.customers.address.Country;
import net.f.restclothing.business.customers.customer.Customer;
import org.junit.Test;
import org.springframework.restdocs.hypermedia.LinksSnippet;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MvcResult;

import java.util.regex.Pattern;

import static net.f.restclothing.business.RestClothingApplication.CURIE_NAMESPACE;
import static net.f.restclothing.business.TestDataProducer.ADMIN_EMAIL;
import static org.hamcrest.Matchers.*;
import static org.springframework.hateoas.MediaTypes.HAL_JSON;
import static org.springframework.http.HttpHeaders.LOCATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.JsonFieldType.NULL;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Artyom Fyodorov
 */
public class CustomerApiDocumentationTest extends AbstractWebIntegrationTest {
    private final String customersPath = "/customers";
    private final LinksSnippet canonicalCustomerLinks = links(
            linkWithRel("self").description("Canonical link for this <<resources-customer,customer>>."),
            linkWithRel(CURIE_NAMESPACE + ":customer").description("This <<resources-customer,customer>>."),
            linkWithRel(CURIE_NAMESPACE + ":update").description("Link to update this <<resources-customer,customer>>."),
            linkWithRel(CURIE_NAMESPACE + ":orders").description("The <<resources-orders,Orders resource>> for this <<resources-customer,customer>>."),
            linkWithRel("curies").description("CUR-ies.")
    );

    @Test
    public void rejectsCreationOfANewCustomerBecauseTheItemsCannotBeShippedToThePresentedCountry() throws Exception {
        Customer customer = TestDataProducer.getCustomer();
        Country country = customer.getAddresses().get(0).getCountry();
        String customerJSON = asJSON(customer);
        customerJSON = customerJSON.replace(country.toString(), "SOVIET_UNION");

        this.mockMvc.perform(
                post(customersPath)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(customerJSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void successfullyCreatesANewCustomer() throws Exception {
        Customer customer = TestDataProducer.getCustomer();
        String customerJSON = asJSON(customer);
        // 1
        this.mockMvc.perform(
                post(customersPath)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(customerJSON))
                .andExpect(status().isCreated())
                .andExpect(header().string(LOCATION, notNullValue()))
                .andDo(document("customer-create",
                        requestFields(
                                fieldWithPath("firstName").description("The first name of the customer."),
                                fieldWithPath("lastName").description("The last name of the customer."),
                                fieldWithPath("email").description("The email address of the customer."),
                                fieldWithPath("password").description("The password of the customer."),
                                fieldWithPath("creditCard.cardNumber").description("The credit card number."),
                                fieldWithPath("creditCard.cardHolderName").description("The cardholder name."),
                                fieldWithPath("creditCard.expiryMonth").description("The expiration month of the credit card."),
                                fieldWithPath("creditCard.expiryYear").description("The expiration year of the credit card."),
                                fieldWithPath("creditCard.type").description("The credit card type."),
                                fieldWithPath("addresses[]").description("An array of addresses."),
                                fieldWithPath("addresses[].street").description("The part of the shipping address."),
                                fieldWithPath("addresses[].city").description("The part of the shipping address."),
                                fieldWithPath("addresses[].state").description("The part of the shipping address."),
                                fieldWithPath("addresses[].country").description("The part of the shipping address."),
                                fieldWithPath("addresses[].zipCode").description("The part of the shipping address.")
                        )));

    }

    @WithUserDetails(ADMIN_EMAIL)
    @Test
    public void getCurrentCustomer() throws Exception {

        this.mockMvc.perform(
                get("/customers/search/current-customer")
                        .accept(HAL_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(HAL_JSON))
                .andExpect(jsonPath("$._links." + CURIE_NAMESPACE + ":customer.href", is(endsWith("/1"))))
                .andDo(document("customer-current-get", canonicalCustomerLinks,
                        responseFields(
                                fieldWithPath("firstName").description("The customer's first name."),
                                fieldWithPath("lastName").description("The customer's last name."),
                                fieldWithPath("email").description("The customer's email address."),
                                fieldWithPath("creditCard").type(NULL).description("The customer's credit card."),
                                fieldWithPath("addresses").description("The customer's shipping address."),
                                subsectionWithPath("_links").description("<<resources-current-customer-links,Links>> to other resources.")
                        )));
    }

    @Test
    public void updatesCustomerPersonalData() throws Exception {
        // 1
        Customer customer = TestDataProducer.getCustomer();
        String customerJSON = asJSON(customer);
        MvcResult mvcResult = this.mockMvc.perform(
                post(customersPath)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(customerJSON))
                .andExpect(status().isCreated())
                .andExpect(header().string(LOCATION, notNullValue()))
                .andReturn();
        // 2
        String location = mvcResult.getResponse().getHeader(LOCATION);
        final Long id = parseId(location);
        // 3
        final String fn = "Joe", ln = "Bloggs";
        customer.setFirstName(fn);
        customer.setLastName(ln);
        customerJSON = asJSON(customer);
        // Test 1
        this.mockMvc.perform(
                put(customersPath + "/" + id)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(customerJSON)
                        .with(httpBasic(customer.getEmail(), customer.getPassword())))
                .andExpect(status().isNoContent())
                .andExpect(header().string(LOCATION, notNullValue()))
                .andDo(document("customer-update",
                        requestFields(
                                fieldWithPath("firstName").description("The first name of the customer."),
                                fieldWithPath("lastName").description("The last name of the customer."),
                                fieldWithPath("email").description("The email address of the customer."),
                                fieldWithPath("password").description("The password of the customer."),
                                fieldWithPath("creditCard.cardNumber").description("The credit card number."),
                                fieldWithPath("creditCard.cardHolderName").description("The cardholder name."),
                                fieldWithPath("creditCard.expiryMonth").description("The expiration month of the credit card."),
                                fieldWithPath("creditCard.expiryYear").description("The expiration year of the credit card."),
                                fieldWithPath("creditCard.type").description("The credit card type."),
                                fieldWithPath("addresses[]").description("An array of addresses."),
                                fieldWithPath("addresses[].street").description("The part of the shipping address."),
                                fieldWithPath("addresses[].city").description("The part of the shipping address."),
                                fieldWithPath("addresses[].state").description("The part of the shipping address."),
                                fieldWithPath("addresses[].country").description("The part of the shipping address."),
                                fieldWithPath("addresses[].zipCode").description("The part of the shipping address.")
                        )));
        // Test 2
        this.mockMvc.perform(
                get(customersPath + "/" + id)
                        .with(httpBasic(customer.getEmail(), customer.getPassword())))
                .andExpect(jsonPath("$.firstName", is(fn)))
                .andExpect(jsonPath("$.lastName", is(ln)))
                .andDo(document("customer-get", canonicalCustomerLinks,
                        responseFields(
                                fieldWithPath("firstName").description("The first name of the customer."),
                                fieldWithPath("lastName").description("The last name of the customer."),
                                fieldWithPath("email").description("The email address of the customer."),
                                fieldWithPath("creditCard.cardNumber").description("The credit card number."),
                                fieldWithPath("addresses[]").description("An array of addresses."),
                                fieldWithPath("addresses[].street").description("The part of the shipping address."),
                                fieldWithPath("addresses[].city").description("The part of the shipping address."),
                                fieldWithPath("addresses[].state").description("The part of the shipping address."),
                                fieldWithPath("addresses[].country").description("The part of the shipping address."),
                                fieldWithPath("addresses[].zipCode").description("The part of the shipping address."),
                                subsectionWithPath("_links").description("<<resources-customer-links,Links>> to other resources.")
                        )));
    }

    private Long parseId(String location) {
        Pattern pattern = Pattern.compile("(\\d+)$");
        java.util.regex.Matcher matcher = pattern.matcher(location);
        matcher.find();
        return Long.parseLong(matcher.group(), 10);
    }

    private String asJSON(Customer c) {
        final Address customerAddress = c.getAddresses().get(0);

        return "{\n" +
                "\t\"firstName\":\"" + c.getFirstName() + "\",\n" +
                "\t\"lastName\":\"" + c.getLastName() + "\",\n" +
                "\t\"email\":\"" + c.getEmail() + "\",\n" +
                "\t\"password\":\"" + c.getPassword() + "\",\n" +
                "\t\"creditCard\":{\n" +
                "\t\t\t\"cardNumber\":\"" + c.getCreditCard().getCardNumber() + "\",\n" +
                "\t\t\t\"cardHolderName\":\"" + c.getCreditCard().getCardHolderName() + "\",\n" +
                "\t\t\t\"expiryMonth\":" + c.getCreditCard().getExpiryMonth().getValue() + ",\n" +
                "\t\t\t\"expiryYear\":" + c.getCreditCard().getExpiryYear().getValue() + ",\n" +
                "\t\t\t\"type\":\"" + c.getCreditCard().getType() + "\"\n" +
                "\t\t},\n" +
                "\t\"addresses\":[\n" +
                "\t\t{\n" +
                "\t\t\t\"street\":\"" + customerAddress.getStreet() + "\",\n" +
                "\t\t\t\"city\":\"" + customerAddress.getCity() + "\",\n" +
                "\t\t\t\"state\":\"" + customerAddress.getState() + "\",\n" +
                "\t\t\t\"country\":\"" + customerAddress.getCountry() + "\",\n" +
                "\t\t\t\"zipCode\":\"" + customerAddress.getZipCode() + "\"\n" +
                "\t\t}\n" +
                "\t]\n" +
                "}";
    }
}