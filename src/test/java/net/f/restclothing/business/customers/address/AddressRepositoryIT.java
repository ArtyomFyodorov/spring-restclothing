package net.f.restclothing.business.customers.address;

import net.f.restclothing.AbstractIntegrationTest;
import net.f.restclothing.business.customers.customer.CustomerRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;

import java.util.List;

import static net.f.restclothing.business.TestDataProducer.CUSTOMER_EMAIL;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Artyom Fyodorov
 */
public class AddressRepositoryIT extends AbstractIntegrationTest {

    @Autowired AddressRepository addressRepository;
    @Autowired CustomerRepository customerRepository;

    @WithUserDetails(CUSTOMER_EMAIL)
    @Test
    public void successfullyFindsAddressesByCurrentCustomer() {

        final List<Address> addressList = addressRepository.findAddressesOfCurrentCustomer();
        assertNotNull(addressList);
        assertTrue(addressList.size() > 0);
    }
}