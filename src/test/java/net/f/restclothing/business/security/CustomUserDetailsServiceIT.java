package net.f.restclothing.business.security;

import net.f.restclothing.AbstractIntegrationTest;
import net.f.restclothing.business.customers.customer.CustomerRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import static net.f.restclothing.business.TestDataProducer.CUSTOMER_EMAIL;
import static net.f.restclothing.business.TestDataProducer.CUSTOMER_PASSWORD;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

/**
 * @author Artyom Fyodorov
 */
public class CustomUserDetailsServiceIT extends AbstractIntegrationTest {

    @Autowired CustomerRepository customerRepository;
    @Autowired CustomUserDetailsService userDetailsService;
    @Autowired PasswordEncoder passwordEncoder;

    @Test
    public void findsAnExistingCustomer() throws Exception {
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(CUSTOMER_EMAIL);
        assertThat(userDetails, is(notNullValue()));
        GrantedAuthority roleUser = new SimpleGrantedAuthority("ROLE_CUSTOMER");
        assertTrue(userDetails.getAuthorities().contains(roleUser));
        assertEquals(userDetails.getUsername(), CUSTOMER_EMAIL);
        assertTrue(passwordEncoder.matches(CUSTOMER_PASSWORD, userDetails.getPassword()));
    }

    @Test(expected = UsernameNotFoundException.class)
    public void doesNotFoundCustomer() throws Exception {
        this.userDetailsService.loadUserByUsername("unknown_email");
    }

}