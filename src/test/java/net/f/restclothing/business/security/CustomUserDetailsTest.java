package net.f.restclothing.business.security;

import net.f.restclothing.business.TestDataProducer;
import net.f.restclothing.business.customers.customer.Customer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;

import static net.f.restclothing.business.TestDataProducer.ADMIN_EMAIL;
import static net.f.restclothing.business.TestDataProducer.CUSTOMER_EMAIL;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.CombinableMatcher.both;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class CustomUserDetailsTest {

    private final GrantedAuthority admin = new SimpleGrantedAuthority("ROLE_ADMIN");
    private final GrantedAuthority customer = new SimpleGrantedAuthority("ROLE_CUSTOMER");
    private Customer customerStub;

    @Before
    public void init_customer() throws Exception {
        this.customerStub = TestDataProducer.getCustomer();
    }

    @Test
    public void customer_has_admin_and_user_roles() throws Exception {
        this.customerStub.setEmail(ADMIN_EMAIL);    //  admin@clothing.com has role ADMIN

        CustomUserDetails userDetails = new CustomUserDetails(customerStub);
        Collection<GrantedAuthority> roles = userDetails.getAuthorities();

        assertThat(roles, hasItems(admin, customer));
    }

    @Test
    public void customer_has_only_user_role() throws Exception {
        this.customerStub.setEmail(CUSTOMER_EMAIL);   //  richard.roe@mail.com does't have ADMIN role

        CustomUserDetails userDetails = new CustomUserDetails(customerStub);
        Collection<GrantedAuthority> roles = userDetails.getAuthorities();

        assertThat(roles, both(hasItem(not(admin))).and(hasItem(customer)));
    }
}