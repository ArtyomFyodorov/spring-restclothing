package net.f.restclothing;

import net.f.restclothing.business.RestClothingApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

/**
 * @author Artyom Fyodorov
 */
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@Transactional
@SpringBootTest(classes = RestClothingApplication.class)
public abstract class AbstractIntegrationTest {
}
