package net.f.restclothing;

import net.f.restclothing.business.TestDataProducer;
import net.f.restclothing.business.customers.address.Address;
import net.f.restclothing.business.orders.order.Order;
import net.f.restclothing.business.orders.product.Product;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.format.FormatterRegistry;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.math.BigDecimal;

/**
 * @author Artyom Fyodorov
 */
@TestConfiguration
public class WebMvcTestConfigurator {

    public static final String PAYMENT_EXPECTED = "23";
    public static final String PAID_ORDER = "42";
    public static final String SHIPPED_ORDER = "47";
    public static final String DELIVERED_ORDER = "69";
    public static final String NON_EXISTENT_ORDER = "666";

    // https://stackoverflow.com/a/40243424
    @Bean
    WebMvcConfigurer configurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addFormatters(FormatterRegistry registry) {
                registry.addConverter(String.class, Order.class, id -> {
                    Product product = new Product();
                    product.setUnitPrice(BigDecimal.ZERO);

                    switch (id) {
                        case PAYMENT_EXPECTED: {
                            Order order = TestDataProducer.getOrder(13L, new Address(), product, 1, .0);
                            ReflectionTestUtils.setField(order, "id", 23L);
                            return order;
                        }
                        case PAID_ORDER: {
                            Order order = TestDataProducer.getOrder(13L, new Address(), product, 1, .0);
                            ReflectionTestUtils.setField(order, "id", 42L);
                            order.markPaid();
                            return order;
                        }
                        case SHIPPED_ORDER: {
                            Order order = TestDataProducer.getOrder(13L, new Address(), product, 1, .0);
                            ReflectionTestUtils.setField(order, "id", 47L);
                            order.markPaid().markShipped();
                            return order;
                        }
                        case DELIVERED_ORDER: {
                            Order order = TestDataProducer.getOrder(13L, new Address(), product, 1, .0);
                            ReflectionTestUtils.setField(order, "id", 69L);
                            order.markPaid().markShipped().markDelivered();
                            return order;
                        }
                        default:
                            return null;
                    }
                });
            }
        };
    }
}
