INSERT INTO customers (version, credit_card_id, email, first_name, last_name, password)
VALUES
  (0, NULL, 'admin@clothing.com', 'Duke', 'Mascot', '$2a$10$8JD3zqRzeCWyYq1uZmmsge4kpVCpel92L9cJafQn7m9LV0rUTV4PG');

INSERT INTO credit_cards (version, holder_name, card_number, expiry_month, expiry_year, type)
VALUES (0, 'Richard Roe', 5104254709030966, 1, 2065, 'VISA');

INSERT INTO customers (version, credit_card_id, email, first_name, last_name, password) VALUES
  (0, 1, 'richard.roe@mail.com', 'Richard', 'Roe', '$2a$10$zOwO2yn/BLL2NBhqb8jnyOqbSYNg1sBsXw5ehwzN4XCBJwj9ER7M.');

INSERT INTO addresses (version, city, country, state, street, zip_code)
VALUES (0, 'St. Petersburg', 'RUSSIAN_FEDERATION', '', '15 Furshtatskaya St.', 191028);

INSERT INTO customers_addresses (customer_id, addresses_id) VALUES (2, 1);
