INSERT INTO customers (version, credit_card_id, email, first_name, last_name, password)
VALUES
  (0, NULL, 'admin@clothing.com', 'Duke', 'Mascot', '$2a$10$8JD3zqRzeCWyYq1uZmmsge4kpVCpel92L9cJafQn7m9LV0rUTV4PG');

INSERT INTO credit_cards (version, holder_name, card_number, expiry_month, expiry_year, type)
VALUES (0, 'Duke Mascot', 1029384756473829, 3, 2075, 'MIR');

INSERT INTO customers (version, credit_card_id, email, first_name, last_name, password) VALUES
  (0, 1, 'duchess.mock@mail.com', 'Duchess', 'Mock', '$2a$10$SXqfFDywB9e4yl59R2tVMe8bTiiAKOo.uKa004tUztppPJJiwsk8C');

INSERT INTO addresses (version, city, country, state, street, zip_code)
VALUES (0, 'Moscow', 'RUSSIAN_FEDERATION', '', 'Bolshoy Deviatinsky Pereulok No. 8', 121099);

INSERT INTO customers_addresses (customer_id, addresses_id) VALUES (2, 1);

INSERT INTO products (version, name, sku, unit_price)
VALUES (0, '4 Pack Bolter Men''s Everyday Cotton Blend Short Sleeve T-shirt, Small', 'B06XSK6ST4', 26.99);
INSERT INTO products (version, name, sku, unit_price)
VALUES
  (0, 'Womens Street Style Feather Pattern T-shirts Casual Loose Top Tee Shirts, Large, Small', 'B072LRZ8KV',
   13.45);
INSERT INTO products (version, name, sku, unit_price)
VALUES (0, 'Southern Marsh Authentic Tee, Medium', 'B01N5VATF3', 30.0);
INSERT INTO products (version, name, sku, unit_price)
VALUES (0, 'Women Summer Pocket Short Sleeve Shirt T-shirt Casual Blouse Tops, Medium', 'B06XGVW5XR', 7.69);

INSERT INTO catalogs (version, name) VALUES (0, 'Clothing Catalog');

INSERT INTO catalogs_products (catalog_id, products_id) VALUES (1, 1);
INSERT INTO catalogs_products (catalog_id, products_id) VALUES (1, 2);
INSERT INTO catalogs_products (catalog_id, products_id) VALUES (1, 3);
INSERT INTO catalogs_products (catalog_id, products_id) VALUES (1, 4);
