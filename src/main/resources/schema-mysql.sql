-- Run this code once on your desired DB:
/*
DROP PROCEDURE IF EXISTS PROC_DROP_FOREIGN_KEY;
DELIMITER $$
CREATE PROCEDURE PROC_DROP_FOREIGN_KEY(IN tableName VARCHAR(64), IN constraintName VARCHAR(64))
  BEGIN
    IF EXISTS(
        SELECT *
        FROM information_schema.table_constraints
        WHERE
          table_schema = DATABASE() AND
          table_name = tableName AND
          constraint_name = constraintName AND
          constraint_type = 'FOREIGN KEY')
    THEN
      SET @query = CONCAT('ALTER TABLE ', tableName, ' DROP FOREIGN KEY ', constraintName, ';');
      PREPARE stmt FROM @query;
      EXECUTE stmt;
      DEALLOCATE PREPARE stmt;
    END IF;
  END$$
DELIMITER ;
*/

CALL PROC_DROP_FOREIGN_KEY('catalogs_products', 'FK_catalogs_products_1');
CALL PROC_DROP_FOREIGN_KEY('catalogs_products', 'FK_catalogs_products_2');
CALL PROC_DROP_FOREIGN_KEY('customers', 'FK_customers_1');
CALL PROC_DROP_FOREIGN_KEY('customers_addresses', 'FK_customers_addresses_1');
CALL PROC_DROP_FOREIGN_KEY('customers_addresses', 'FK_customers_addresses_2');
CALL PROC_DROP_FOREIGN_KEY('invoices', 'FK_invoices_1');
CALL PROC_DROP_FOREIGN_KEY('line_items', 'FK_line_items_1');
CALL PROC_DROP_FOREIGN_KEY('orders', 'FK_orders_1');
CALL PROC_DROP_FOREIGN_KEY('orders_line_items', 'FK_orders_line_items_1');
CALL PROC_DROP_FOREIGN_KEY('orders_line_items', 'FK_orders_line_items_2');
CALL PROC_DROP_FOREIGN_KEY('payments', 'FK_payments_1');

DROP TABLE IF EXISTS addresses;
DROP TABLE IF EXISTS catalogs;
DROP TABLE IF EXISTS catalogs_products;
DROP TABLE IF EXISTS credit_cards;
DROP TABLE IF EXISTS customers;
DROP TABLE IF EXISTS customers_addresses;
DROP TABLE IF EXISTS invoices;
DROP TABLE IF EXISTS line_items;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS orders_line_items;
DROP TABLE IF EXISTS payments;
DROP TABLE IF EXISTS products;

CREATE TABLE addresses (
  id       BIGINT       NOT NULL AUTO_INCREMENT,
  version  INT,
  city     VARCHAR(100) NOT NULL,
  country  VARCHAR(100) NOT NULL,
  state    VARCHAR(100),
  street   VARCHAR(255) NOT NULL,
  zip_code INTEGER      NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE catalogs (
  id      BIGINT       NOT NULL AUTO_INCREMENT,
  version INT,
  name    VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE catalogs_products (
  catalog_id  BIGINT NOT NULL,
  products_id BIGINT NOT NULL,
  PRIMARY KEY (catalog_id, products_id)
);

CREATE TABLE credit_cards (
  id           BIGINT       NOT NULL AUTO_INCREMENT,
  version      INT,
  holder_name  VARCHAR(100) NOT NULL,
  card_number  VARCHAR(16)  NOT NULL,
  expiry_month INTEGER      NOT NULL,
  expiry_year  INTEGER      NOT NULL,
  type         VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE customers (
  id             BIGINT       NOT NULL AUTO_INCREMENT,
  version        INT,
  email          VARCHAR(255) NOT NULL,
  first_name     VARCHAR(100) NOT NULL,
  last_name      VARCHAR(100) NOT NULL,
  password       VARCHAR(60) NOT NULL,
  credit_card_id BIGINT,
  PRIMARY KEY (id)
);

CREATE TABLE customers_addresses (
  customer_id  BIGINT NOT NULL,
  addresses_id BIGINT NOT NULL
);

CREATE TABLE invoices (
  id                BIGINT NOT NULL AUTO_INCREMENT,
  version           INT,
  amount_of_invoice DECIMAL(19, 2),
  customer_id   BIGINT NOT NULL,
  status            VARCHAR(100),
  order_id          BIGINT,
  PRIMARY KEY (id)
);

CREATE TABLE line_items (
  id         BIGINT  NOT NULL AUTO_INCREMENT,
  version    INT,
  amount     DECIMAL(19, 2),
  quantity   INTEGER NOT NULL,
  unit_price DECIMAL(19, 2),
  product_id BIGINT  NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE orders (
  id                  BIGINT NOT NULL AUTO_INCREMENT,
  version             INT,
  customer_id     BIGINT NOT NULL,
  status              VARCHAR(100),
  total_amount        DECIMAL(19, 2),
  shipping_address_id BIGINT,
  PRIMARY KEY (id)
);

CREATE TABLE orders_line_items (
  order_id      BIGINT NOT NULL,
  line_items_id BIGINT NOT NULL,
  PRIMARY KEY (order_id, line_items_id)
);

CREATE TABLE payments (
  id             BIGINT   NOT NULL AUTO_INCREMENT,
  version        INT,
  payment_date   TINYBLOB NOT NULL,
  total_amount   DECIMAL(19, 2),
  credit_card_id BIGINT,
  PRIMARY KEY (id)
);

CREATE TABLE products (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  version    INT,
  name       VARCHAR(255),
  sku        VARCHAR(10),
  unit_price DECIMAL(19, 2),
  PRIMARY KEY (id)
);

ALTER TABLE catalogs
  ADD CONSTRAINT UK_catalogs_1 UNIQUE (name);

ALTER TABLE catalogs_products
  ADD CONSTRAINT UK_catalogs_products_1 UNIQUE (products_id);

ALTER TABLE customers
  ADD CONSTRAINT UK_customers_1 UNIQUE (email);

ALTER TABLE customers_addresses
  ADD CONSTRAINT UK_customers_addresses_1 UNIQUE (addresses_id);

ALTER TABLE orders_line_items
  ADD CONSTRAINT UK_orders_line_items_1 UNIQUE (line_items_id);

ALTER TABLE catalogs_products
  ADD CONSTRAINT FK_catalogs_products_1 FOREIGN KEY (products_id) REFERENCES products (id);

ALTER TABLE catalogs_products
  ADD CONSTRAINT FK_catalogs_products_2 FOREIGN KEY (catalog_id) REFERENCES catalogs (id);

ALTER TABLE customers
  ADD CONSTRAINT FK_customers_1 FOREIGN KEY (credit_card_id) REFERENCES credit_cards (id);

ALTER TABLE customers_addresses
  ADD CONSTRAINT FK_customers_addresses_1 FOREIGN KEY (addresses_id) REFERENCES addresses (id);

ALTER TABLE customers_addresses
  ADD CONSTRAINT FK_customers_addresses_2 FOREIGN KEY (customer_id) REFERENCES customers (id);

ALTER TABLE invoices
  ADD CONSTRAINT FK_invoices_1 FOREIGN KEY (order_id) REFERENCES orders (id);

ALTER TABLE line_items
  ADD CONSTRAINT FK_line_items_1 FOREIGN KEY (product_id) REFERENCES products (id);

ALTER TABLE orders
  ADD CONSTRAINT FK_orders_1 FOREIGN KEY (shipping_address_id) REFERENCES addresses (id);

ALTER TABLE orders_line_items
  ADD CONSTRAINT FK_orders_line_items_1 FOREIGN KEY (line_items_id) REFERENCES line_items (id);

ALTER TABLE orders_line_items
  ADD CONSTRAINT FK_orders_line_items_2 FOREIGN KEY (order_id) REFERENCES orders (id);

ALTER TABLE payments
  ADD CONSTRAINT FK_payments_1 FOREIGN KEY (credit_card_id) REFERENCES credit_cards (id);
