package net.f.restclothing.business.base;

import lombok.extern.slf4j.Slf4j;
import net.f.restclothing.business.customers.address.exceptions.AddressNotFoundException;
import net.f.restclothing.business.customers.creditcard.exceptions.PaymentException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author Artyom Fyodorov
 */
@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(AddressNotFoundException.class)
    protected ResponseEntity<?> handleAddressNotFoundException(RuntimeException ex, ServletWebRequest webRequest) {
        log.info("'handleAddressNotFoundException' Bad Request: {}", ex.getMessage());

        final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        final ApiError apiError = ApiError.create(httpStatus, ex.getLocalizedMessage(),
                webRequest.getRequest().getServletPath());

        return handleExceptionInternal(ex, apiError,
                new HttpHeaders(), httpStatus, webRequest);
    }

    @ExceptionHandler(PaymentException.class)
    protected ResponseEntity<?> handlePaymentException(RuntimeException ex, ServletWebRequest webRequest) {
        log.info("'handlePaymentException' Bad Request: {}", ex.getMessage());

        final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        final ApiError apiError = ApiError.create(httpStatus, ex.getLocalizedMessage(),
                webRequest.getRequest().getServletPath());

        return handleExceptionInternal(ex, apiError,
                new HttpHeaders(), httpStatus, webRequest);
    }
}
