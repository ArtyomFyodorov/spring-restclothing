package net.f.restclothing.business.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;

/**
 * @author Artyom Fyodorov
 */
@MappedSuperclass
@Access(AccessType.FIELD)
@Getter
@ToString
@EqualsAndHashCode
public class AbstractEntity implements Identifiable<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private final Long id;
    @Version
    @JsonIgnore
    private Integer version;

    public AbstractEntity() {
        this.id = null;
    }
}
