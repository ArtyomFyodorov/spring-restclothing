package net.f.restclothing.business.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Date;

/**
 * @author Artyom Fyodorov
 */
@Getter
public final class ApiError {

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private final Date timestamp;
    private final int status;
    private final String error;
    private final String message;
    private final String path;

    private ApiError(HttpStatus httpStatus, String message, String path) {
        this.timestamp = new Date();
        this.error = httpStatus.getReasonPhrase();
        this.status = httpStatus.value();
        this.message = message;
        this.path = path;
    }

    public static ApiError create(HttpStatus httpStatus, String message, String path) {
        return new ApiError(httpStatus, message, path);
    }
}
