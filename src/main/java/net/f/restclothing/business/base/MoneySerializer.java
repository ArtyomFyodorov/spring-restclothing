package net.f.restclothing.business.base;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author Artyom Fyodorov
 */
public class MoneySerializer extends JsonSerializer<BigDecimal> {
    @Override
    public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (value != null) {
//            gen.writeString(this.getLocaleSpecificSerializedValue(value, LocaleContextHolder.getLocale()));
            gen.writeString(this.getLocaleSpecificSerializedValue(value, Locale.US)); // todo: stub
        } else {
            gen.writeNull();
        }
    }

    private String getLocaleSpecificSerializedValue(BigDecimal value, Locale locale) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        return numberFormat.format(value);
    }
}
