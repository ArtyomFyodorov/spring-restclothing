package net.f.restclothing.business.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.domain.AfterDomainEventPublication;
import org.springframework.data.domain.DomainEvents;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Artyom Fyodorov
 */
public class AbstractAggregateRoot extends AbstractEntity {

    //    @Getter(onMethod = @__(@DomainEvents))
    private final transient List<Object> domainEvents = new ArrayList<>();

    protected <T> T registerEvent(T event) {
        Assert.notNull(event, "Domain event must not be null!");
        this.domainEvents.add(event);

        return event;
    }

    @JsonIgnore
    @AfterDomainEventPublication
    public void clearDomainEvents() {
        this.domainEvents.clear();
    }

    @JsonIgnore
    @DomainEvents
    public List<Object> getDomainEvents() {
        return this.domainEvents;
    }
}