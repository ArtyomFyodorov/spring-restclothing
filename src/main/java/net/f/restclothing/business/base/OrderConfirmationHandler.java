package net.f.restclothing.business.base;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.f.restclothing.business.orders.order.Order;
import net.f.restclothing.business.orders.order.OrderPaidEvent;
import net.f.restclothing.business.orders.order.OrderRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * The {@link OrderPaidEvent} listener simulates the process
 * of delivery of the paid order by marking its status after 10 and 60 seconds.
 *
 * @author Artyom Fyodorov
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class OrderConfirmationHandler {

    private final @NonNull OrderRepository orderRepository;

    @Async
    @TransactionalEventListener
    public void handleOrderConfirmationEvent(OrderPaidEvent event) {

        final Long orderId = event.getOrderId();
        final Long customerId = event.getCustomerId();
        Order order = orderRepository
                .findByIdAndCustomerId(orderId, customerId).orElseThrow(() ->
                        new IllegalArgumentException(
                                String.format("No order found for order id %d and customer id %d%n", orderId, customerId))
                );

        log.info("Starting to process order: {}.", order);
        sleep(10_000);
        order = orderRepository.save(order.markShipped());

        log.info("Order shipped: {}.", order);
        sleep(60_000);
        order = orderRepository.save(order.markDelivered());

        log.info("Order delivered: {}.", order);
        log.info("Finished processing order: {}.", order);
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
