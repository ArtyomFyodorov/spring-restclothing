package net.f.restclothing.business.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.f.restclothing.business.customers.customer.Customer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * @author Artyom Fyodorov
 */
public class CustomUserDetails implements UserDetails {
    private static final long serialVersionUID = -5764129020834316564L;
    private final Long id;
    private final String username;
    private final String password;
    private final Collection<GrantedAuthority> roles;

    public CustomUserDetails(Customer customer) {
        this.id = customer.getId();
        this.username = customer.getEmail();
        this.password = customer.getPassword();
        this.roles = identifyRoles(customer);

    }

    private Collection<GrantedAuthority> identifyRoles(Customer customer) {
        SimpleGrantedAuthority customerRole = new SimpleGrantedAuthority("ROLE_CUSTOMER");
        SimpleGrantedAuthority adminRole = new SimpleGrantedAuthority("ROLE_ADMIN");
        Collection<GrantedAuthority> customerRoleAsCollection = Collections.singletonList(customerRole);
        Collection<GrantedAuthority> combinedRoles = Arrays.asList(customerRole, adminRole);

        for (Role role : Role.values()) {
            if (role.getName().equalsIgnoreCase(customer.getEmail())) {
                return combinedRoles;
            }
        }

        return customerRoleAsCollection;
    }

    public Long getId() {
        return id;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Getter
    @AllArgsConstructor
    private enum Role {
        ADMIN("admin@clothing.com");

        private final String name;
    }
}
