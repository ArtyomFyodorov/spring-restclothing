package net.f.restclothing.business.security;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.f.restclothing.business.customers.customer.Customer;
import net.f.restclothing.business.customers.customer.CustomerRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author Artyom Fyodorov
 */
@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final @NonNull CustomerRepository customerRepository;

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public UserDetails loadUserByUsername(String name) {
        log.info("'loadUserByUsername' name: {}", name);
        Optional<Customer> customerMaybe = this.customerRepository.findByEmail(name);

        if (customerMaybe.isPresent()) {
            Customer wellKnownCustomer = customerMaybe.get();
            log.info("'loadUserByUsername' customer was found: {}", wellKnownCustomer);
            return new CustomUserDetails(wellKnownCustomer);
        } else {
            log.info("'loadUserByUsername' customer with name {} was not found", name);
            throw new UsernameNotFoundException("Unable to load user by name: " + name);
        }
    }

}
