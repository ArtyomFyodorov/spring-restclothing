package net.f.restclothing.business.payments.payment;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.f.restclothing.business.base.MoneySerializer;
import net.f.restclothing.business.customers.creditcard.CreditCard;
import net.f.restclothing.business.orders.order.Order;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.security.Principal;

/**
 * @author Artyom Fyodorov
 */
@Controller
@RequestMapping("/orders/{id}")
@RequiredArgsConstructor
public class PaymentController {
    private final @NonNull PaymentService paymentService;
    private final @NonNull EntityLinks entityLinks;

    @PutMapping(path = PaymentLinks.PAYMENT)
    public ResponseEntity<?> submitPayment(@PathVariable("id") Order order, Principal principal) {
        if (order == null || order.isPaid()) {
            return ResponseEntity.notFound().build();
        }

        String principalName = principal.getName(); // email
        Payment payment = paymentService.confirmAndPay(order, principalName);
        PaymentResource resource = createPaymentResource(order, payment);

        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    private PaymentResource createPaymentResource(Order order, Payment payment) {
        PaymentResource resource = new PaymentResource(payment.getTotalAmount(), payment.getCreditCard());
        resource.add(entityLinks.linkToSingleResource(order));
        return resource;
    }

    @Data
    @EqualsAndHashCode(callSuper = true)
    private static class PaymentResource extends ResourceSupport {
        @JsonSerialize(using = MoneySerializer.class)
        private final BigDecimal totalAmount;
        private final CreditCard creditCard;
    }
}
