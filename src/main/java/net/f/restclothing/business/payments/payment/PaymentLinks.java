package net.f.restclothing.business.payments.payment;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.f.restclothing.business.RestClothingApplication;
import net.f.restclothing.business.orders.order.Order;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

/**
 * @author Artyom Fyodorov
 */
@Component
@RequiredArgsConstructor
public class PaymentLinks {
    public static final String PAYMENT = "/payment";
    public static final String PAYMENT_REL = RestClothingApplication.CURIE_NAMESPACE + ":payment";

    private final @NonNull EntityLinks entityLinks;

    public Link getPaymentLink(Order order) {
        return entityLinks.linkForSingleResource(Order.class, order.getId())
                .slash(PAYMENT).withRel(PAYMENT_REL);
    }
}
