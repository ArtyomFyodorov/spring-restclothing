package net.f.restclothing.business.payments.payment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author Artyom Fyodorov
 */
@RepositoryRestResource(exported = false)
public interface PaymentRepository extends CrudRepository<Payment, Long> {
}
