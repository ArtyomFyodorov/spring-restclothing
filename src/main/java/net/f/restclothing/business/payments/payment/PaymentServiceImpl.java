package net.f.restclothing.business.payments.payment;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.f.restclothing.business.customers.creditcard.CreditCard;
import net.f.restclothing.business.customers.creditcard.exceptions.PaymentException;
import net.f.restclothing.business.customers.customer.Customer;
import net.f.restclothing.business.customers.customer.CustomerRepository;
import net.f.restclothing.business.orders.invoice.Invoice;
import net.f.restclothing.business.orders.invoice.InvoiceRepository;
import net.f.restclothing.business.orders.order.Order;
import net.f.restclothing.business.orders.order.OrderRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static java.lang.String.format;

/**
 * @author Artyom Fyodorov
 */
@Service
@Transactional
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

    private final @NonNull CustomerRepository customerRepository;
    private final @NonNull OrderRepository orderRepository;
    private final @NonNull InvoiceRepository invoiceRepository;
    private final @NonNull PaymentRepository paymentRepository;

    @Override
    public Payment confirmAndPay(Order order, String principalName) {
        if (order.isPaid()) {
            throw new PaymentException(order, "The order has already been paid.");
        }

        Optional<Customer> customerMaybe = customerRepository.findByEmail(principalName);
        if (!customerMaybe.isPresent()) {
            throw new UsernameNotFoundException("Unable to load user by name: " + principalName);
        }

        Customer customer = customerMaybe.get();
        CreditCard creditCard = Optional.ofNullable(customer.getCreditCard())
                .orElseThrow(() -> new PaymentException(order, "Credit card not found."));
        if (!creditCard.isValid()) {
            throw new PaymentException(order,
                    format("Credit card number: **** **** **** %s, expired %s is not valid.",
                            creditCard.getCardNumber().substring(12, 16), creditCard.getExpirationDate()));
        }

        orderRepository.save(order.markPaid());
        Invoice invoice = Invoice.create(order);
        invoiceRepository.save(invoice.markPaid());

        return this.paymentRepository.save(Payment.create(creditCard, invoice.getAmountOfInvoice()/*, invoice.getCurrency()*/));
    }


}
