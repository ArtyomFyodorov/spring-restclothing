package net.f.restclothing.business.payments.payment;

import net.f.restclothing.business.orders.order.Order;

/**
 * @author Artyom Fyodorov
 */
public interface PaymentService {
    Payment confirmAndPay(Order order, String principalName);
}
