package net.f.restclothing.business.payments.payment;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.f.restclothing.business.base.AbstractEntity;
import net.f.restclothing.business.customers.creditcard.CreditCard;
import org.springframework.util.Assert;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "payments")
@Getter
@NoArgsConstructor
@ToString(callSuper = true)
public class Payment extends AbstractEntity {

    private BigDecimal totalAmount;
    private LocalDateTime paymentDate;

    @OneToOne
    private @Valid CreditCard creditCard;

    private Payment(CreditCard creditCard, BigDecimal totalAmount) {
        Assert.notNull(creditCard, "Credit card must not be null");
        Assert.notNull(totalAmount, "Total amount must not be null");
        this.creditCard = creditCard;
        this.paymentDate = LocalDateTime.now();
        this.totalAmount = totalAmount;
    }

    public static Payment create(CreditCard creditCard, BigDecimal totalAmount) {
        return new Payment(creditCard, totalAmount);
    }
}
