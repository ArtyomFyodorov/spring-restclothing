package net.f.restclothing.business.orders.product;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import net.f.restclothing.business.base.AbstractEntity;
import net.f.restclothing.business.base.MoneySerializer;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "products")
@Getter @Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"name", "sku", "unitPrice"}, callSuper = false)
@ToString(callSuper = true)
public class Product extends AbstractEntity {

    private @NotBlank String name;
    private @NotBlank String sku;
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal unitPrice;

    public Product(String name, String sku, double unitPrice) {
        this.name = name;
        this.sku = sku;
        this.unitPrice = BigDecimal.valueOf(unitPrice);
    }
}
