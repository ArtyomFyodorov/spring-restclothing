package net.f.restclothing.business.orders.product;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

/**
 * @author Artyom Fyodorov
 */
@Component
@RequiredArgsConstructor
public class ProductOrderResourceProcessor implements ResourceProcessor<Resource<Product>> {

    private final @NonNull ProductLinks productLinks;

    @Override
    public Resource<Product> process(Resource<Product> productResource) {
        Product content = productResource.getContent();
        productResource.add(productLinks.getOrderLink(content));

        return productResource;
    }
}
