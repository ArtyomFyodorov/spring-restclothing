package net.f.restclothing.business.orders.catalog;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

/**
 * @author Artyom Fyodorov
 */
public interface CatalogRepository extends CrudRepository<Catalog, Long> {
    @RestResource(exported = false)
    Optional<Catalog> findByName(String name);

    @Override
    Optional<Catalog> findById(Long aLong);

    @Override
    Iterable<Catalog> findAll();

    @RestResource(exported = false)
    @Override
    <S extends Catalog> S save(S s);

    @RestResource(exported = false)
    @Override
    <S extends Catalog> Iterable<S> saveAll(Iterable<S> iterable);

    @RestResource(exported = false)
    @Override
    boolean existsById(Long aLong);

    @RestResource(exported = false)
    @Override
    Iterable<Catalog> findAllById(Iterable<Long> iterable);

    @RestResource(exported = false)
    @Override
    long count();

    @RestResource(exported = false)
    @Override
    void deleteById(Long aLong);

    @RestResource(exported = false)
    @Override
    void delete(Catalog catalog);

    @RestResource(exported = false)
    @Override
    void deleteAll(Iterable<? extends Catalog> iterable);

    @RestResource(exported = false)
    @Override
    void deleteAll();
}
