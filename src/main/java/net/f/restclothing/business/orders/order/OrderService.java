package net.f.restclothing.business.orders.order;

import net.f.restclothing.business.orders.product.Product;

/**
 * @author Artyom Fyodorov
 */
public interface OrderService {
    void makeOrder(Product product, int quantity, int indexOfAddress, Long customerId);
}
