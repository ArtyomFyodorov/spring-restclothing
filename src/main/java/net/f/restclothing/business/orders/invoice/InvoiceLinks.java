package net.f.restclothing.business.orders.invoice;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.f.restclothing.business.RestClothingApplication;
import net.f.restclothing.business.orders.order.Order;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

/**
 * @author Artyom Fyodorov
 */
@Component
@RequiredArgsConstructor
public class InvoiceLinks {
    public static final String INVOICE = "/invoice";
    public static final String INVOICE_REL = RestClothingApplication.CURIE_NAMESPACE + ":invoice";

    private final @NonNull EntityLinks entityLinks;

    public Link getInvoiceLink(Order order) {
        return this.entityLinks.linkForSingleResource(Order.class, order.getId())
                .slash(INVOICE).withRel(INVOICE_REL);
    }
}
