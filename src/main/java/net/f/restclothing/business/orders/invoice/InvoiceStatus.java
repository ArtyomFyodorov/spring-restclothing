package net.f.restclothing.business.orders.invoice;

/**
 * @author Artyom Fyodorov
 */
public enum InvoiceStatus {
    CREATED, PAID
}
