package net.f.restclothing.business.orders.catalog;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.f.restclothing.business.base.AbstractEntity;
import net.f.restclothing.business.orders.product.Product;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "catalogs")
@Getter
@NoArgsConstructor
@ToString(callSuper = true)
public class Catalog extends AbstractEntity {

    private @NotBlank String name;

    @OneToMany
    private Set<Product> products = new HashSet<>();

    public Catalog(String name, Collection<Product> products) {
        this.name = name;
        this.products.addAll(products);
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public int numberOfProducts() {
        return products.size();
    }
}
