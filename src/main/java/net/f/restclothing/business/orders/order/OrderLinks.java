package net.f.restclothing.business.orders.order;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

/**
 * @author Artyom Fyodorov
 */
@Component
@RequiredArgsConstructor
public class OrderLinks {
    static final String CANCEL = "/cancel";
    static final String CANCEL_REL = "cancel";

    private final @NonNull EntityLinks entityLinks;

    public Link getCancelLink(Order order) {
        return entityLinks.linkForSingleResource(Order.class, order.getId())
                .slash(CANCEL).withRel(CANCEL_REL);
    }
}
