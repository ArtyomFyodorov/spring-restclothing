package net.f.restclothing.business.orders.order;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Artyom Fyodorov
 */
@Controller
@RequestMapping("/orders/{id}")
@RequiredArgsConstructor
public class OrderController {

    private final @NonNull OrderRepository orderRepository;

    @DeleteMapping(path = OrderLinks.CANCEL)
    public ResponseEntity<?> cancellation(@PathVariable("id") Long orderId) {

        return orderRepository.findById(orderId)
                .filter(order -> !order.isPaid())
                .map(order -> {
                    orderRepository.delete(order);
                    return ResponseEntity.noContent().build();
                })
                .orElse(ResponseEntity.notFound().build());
    }
}
