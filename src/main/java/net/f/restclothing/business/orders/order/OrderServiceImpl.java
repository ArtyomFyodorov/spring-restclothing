package net.f.restclothing.business.orders.order;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.f.restclothing.business.customers.address.Address;
import net.f.restclothing.business.customers.address.AddressRepository;
import net.f.restclothing.business.customers.address.exceptions.AddressNotFoundException;
import net.f.restclothing.business.orders.product.Product;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static net.f.restclothing.business.orders.order.OrderStatus.PAYMENT_EXPECTED;

/**
 * @author Artyom Fyodorov
 */
@Service
@Transactional
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final @NonNull OrderRepository orderRepository;
    private final @NonNull AddressRepository addressRepository;

    @Override
    public void makeOrder(Product product, int quantity, int indexOfAddress, Long customerId) {
        final Address shippingAddress =
                getShippingAddressOfCustomerByIndex(indexOfAddress);
        Order unpaidOrder = orderRepository
                .findByCustomerIdAndStatus(customerId, PAYMENT_EXPECTED)
                .orElseGet(() -> Order.create(customerId, shippingAddress));
        unpaidOrder.addLineItem(LineItem.create(product, quantity));
        orderRepository.save(unpaidOrder);
    }

    private Address getShippingAddressOfCustomerByIndex(int indexOfAddress) {
        final List<Address> addresses = addressRepository.findAddressesOfCurrentCustomer();

        if (addresses.size() <= indexOfAddress) {  // indexOfAddress always >= 0
            throw new AddressNotFoundException("Can't find address by index " + indexOfAddress);
        }

        return addresses.get(indexOfAddress);
    }
}
