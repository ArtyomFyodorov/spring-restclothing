package net.f.restclothing.business.orders.invoice;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.f.restclothing.business.base.AbstractEntity;
import net.f.restclothing.business.base.MoneySerializer;
import net.f.restclothing.business.orders.order.Order;
import org.springframework.util.Assert;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "invoices")
@Getter
@NoArgsConstructor
@ToString(callSuper = true)
public class Invoice extends AbstractEntity {

    private Long customerId;
    @Enumerated(EnumType.STRING)
    private @Setter InvoiceStatus status;
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal amountOfInvoice;

    @OneToOne
    private Order order;

    private Invoice(Order order) {
        Assert.notNull(order, "Invoice must contain order");
        this.customerId = order.getCustomerId();
        this.status = InvoiceStatus.CREATED;
        this.order = order;
        this.amountOfInvoice = order.getTotalAmount();
    }

    public static Invoice create(Order order) {
        return new Invoice(order);
    }

    public Invoice markPaid() {
        if (isPaid()) {
            throw new IllegalStateException("Already paid, Invoice amount cannot be paid again!");
        }
        this.status = InvoiceStatus.PAID;
        return this;
    }

    @JsonIgnore
    public boolean isPaid() {
        return !Objects.equals(getStatus(), InvoiceStatus.CREATED);
    }
}
