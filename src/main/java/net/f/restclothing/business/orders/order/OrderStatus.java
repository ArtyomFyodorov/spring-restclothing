package net.f.restclothing.business.orders.order;

/**
 * @author Artyom Fyodorov
 */
public enum OrderStatus {
    PAYMENT_EXPECTED, PAID, SHIPPED, DELIVERED
}
