package net.f.restclothing.business.orders.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import net.f.restclothing.business.base.AbstractAggregateRoot;
import net.f.restclothing.business.base.MoneySerializer;
import net.f.restclothing.business.customers.address.Address;
import org.springframework.util.Assert;

import javax.persistence.*;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "orders")
@Getter
@NoArgsConstructor @AllArgsConstructor(staticName = "create")
@ToString(callSuper = true)
public class Order extends AbstractAggregateRoot {

    @Enumerated(EnumType.STRING)
    private OrderStatus status;
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal totalAmount;
    private @Setter Long customerId;

    @OneToOne
    private @Valid Address shippingAddress;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<LineItem> lineItems = new HashSet<>();

    private Order(Long customerId, Address shippingAddress) {
        Assert.notNull(customerId, "Customer id must not be null");
        Assert.notNull(shippingAddress, "Shipping address must not be null");
        this.customerId = customerId;
        this.shippingAddress = shippingAddress;
        this.status = OrderStatus.PAYMENT_EXPECTED;
    }

    public static Order create(Long customerId, Address shippingAddress) {
        return new Order(customerId, shippingAddress);
    }

    @PrePersist
    @PreUpdate
    void calculateTotalAmount() {
        this.totalAmount = lineItems.stream()
                .map(LineItem::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .setScale(2, RoundingMode.HALF_UP);
    }

    public Order markPaid() {
        if (isPaid()) {
            throw new IllegalStateException("Already paid order cannot be paid again.");
        }

        this.status = OrderStatus.PAID;

        this.registerEvent(OrderPaidEvent.create(getId(), getCustomerId()));

        return this;
    }

    public Order markShipped() {
        if (!isPaid()) {
            throw new IllegalStateException(
                    "Order must be in state payed to start shipping. Current status: " + getStatus());
        }

        this.status = OrderStatus.SHIPPED;
        return this;
    }

    public Order markDelivered() {
        if (!isShipped()) {
            throw new IllegalStateException(
                    "Cannot mark Order as delivered that is currently not shipped. Current status: " + getStatus());
        }

        this.status = OrderStatus.DELIVERED;

        return this;
    }

    @JsonIgnore
    public boolean isPaid() {
        return !Objects.equals(getStatus(), OrderStatus.PAYMENT_EXPECTED);
    }

    @JsonIgnore
    public boolean isShipped() {
        return Objects.equals(getStatus(), OrderStatus.SHIPPED);
    }

    @JsonIgnore
    public boolean isDelivered() {
        return Objects.equals(getStatus(), OrderStatus.DELIVERED);
    }

    public void addLineItem(LineItem candidate) {
        if (!lineItems.contains(candidate)) {
            lineItems.add(candidate);
            return;
        }

        for (LineItem li : lineItems) {
            if (li.equals(candidate)) {
                final int oldQuantity = li.getQuantity();
                final int newQuantity = oldQuantity + candidate.getQuantity();
                candidate.setQuantity(newQuantity);
                lineItems.remove(li);
                lineItems.add(candidate);
                return;
            }
        }
    }

    public int numberOfItems() {
        return lineItems.size();
    }
}
