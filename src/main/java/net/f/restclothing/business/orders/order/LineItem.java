package net.f.restclothing.business.orders.order;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import net.f.restclothing.business.base.AbstractEntity;
import net.f.restclothing.business.base.MoneySerializer;
import net.f.restclothing.business.orders.product.Product;
import org.springframework.util.Assert;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.math.BigDecimal.valueOf;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "line_items")
@Getter
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(of = "product", callSuper = false)
public class LineItem extends AbstractEntity {

    private @Setter int quantity;
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal unitPrice;
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal amount;

    @OneToOne
    private Product product;

    private LineItem(Product product, int quantity) {
        Assert.notNull(product, "Line item must contain product");
        this.product = product;
        this.quantity = quantity;
        this.unitPrice = product.getUnitPrice();
    }

    public static LineItem create(Product product, int quantity) {
        return new LineItem(product, quantity);
    }

    private BigDecimal calculateAmount() {
        this.amount = unitPrice
                .multiply(valueOf(quantity))
                .setScale(2, RoundingMode.HALF_UP);

        return amount;
    }

    public BigDecimal getAmount() {
        return calculateAmount();
    }
}
