package net.f.restclothing.business.orders.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

/**
 * @author Artyom Fyodorov
 */
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    @Override
    Page<Product> findAll(Pageable pageable);

    @Override
    Optional<Product> findById(Long aLong);

    @RestResource(exported = false)
    @Override
    Iterable<Product> findAll(Sort sort);

    @RestResource(exported = false)
    @Override
    <S extends Product> S save(S s);

    @RestResource(exported = false)
    @Override
    <S extends Product> Iterable<S> saveAll(Iterable<S> iterable);

    @RestResource(exported = false)
    @Override
    boolean existsById(Long aLong);

    @RestResource(exported = false)
    @Override
    Iterable<Product> findAll();

    @RestResource(exported = false)
    @Override
    Iterable<Product> findAllById(Iterable<Long> iterable);

    @RestResource(exported = false)
    @Override
    long count();

    @RestResource(exported = false)
    @Override
    void deleteById(Long aLong);

    @RestResource(exported = false)
    @Override
    void delete(Product product);

    @RestResource(exported = false)
    @Override
    void deleteAll(Iterable<? extends Product> iterable);

    @RestResource(exported = false)
    @Override
    void deleteAll();
}
