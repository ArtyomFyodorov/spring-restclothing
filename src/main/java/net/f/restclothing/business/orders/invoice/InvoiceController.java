package net.f.restclothing.business.orders.invoice;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.f.restclothing.business.orders.order.Order;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Artyom Fyodorov
 */
@Controller
@RequestMapping("/orders/{id}")
@RequiredArgsConstructor
public class InvoiceController {

    private final @NonNull InvoiceRepository invoiceRepository;
    private final @NonNull EntityLinks entityLinks;

    @GetMapping(path = InvoiceLinks.INVOICE, produces = {
            MediaTypes.HAL_JSON_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<?> showInvoice(@PathVariable("id") Order order) {

        if (order == null || (!order.isShipped() && !order.isDelivered())) {
            return ResponseEntity.notFound().build();
        }

        return invoiceRepository.findByOrder(order)
                .map(invoice -> ResponseEntity.ok(createInvoiceResource(invoice)))
                .orElse(ResponseEntity.notFound().build());
    }

    private Resource<Invoice> createInvoiceResource(Invoice invoice) {
        Order order = invoice.getOrder();
        Resource<Invoice> resource = new Resource<>(invoice);
        resource.add(entityLinks.linkToSingleResource(order));

        return resource;
    }
}
