package net.f.restclothing.business.orders.order;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.f.restclothing.business.orders.invoice.InvoiceLinks;
import net.f.restclothing.business.payments.payment.PaymentLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

/**
 * @author Artyom Fyodorov
 */
@Component
@RequiredArgsConstructor
public class OrderResourceProcessor implements ResourceProcessor<Resource<Order>> {

    private final @NonNull OrderLinks orderLinks;
    private final @NonNull PaymentLinks paymentLinks;
    private final @NonNull InvoiceLinks invoiceLinks;

    @Override
    public Resource<Order> process(Resource<Order> orderResource) {
        Order content = orderResource.getContent();
        if (!content.isPaid()) {
            orderResource.add(orderLinks.getCancelLink(content));
            orderResource.add(paymentLinks.getPaymentLink(content));
        } else if (content.isShipped() || content.isDelivered()) {
            orderResource.add(invoiceLinks.getInvoiceLink(content));
        }

        return orderResource;
    }
}
