package net.f.restclothing.business.orders.order;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * @author Artyom Fyodorov
 */
@Getter
@RequiredArgsConstructor(staticName = "create")
@EqualsAndHashCode
@ToString
public class OrderPaidEvent {
    private final Long orderId;
    private final Long customerId;

}
