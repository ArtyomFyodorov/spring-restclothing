package net.f.restclothing.business.orders.invoice;

import net.f.restclothing.business.orders.order.Order;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

/**
 * @author Artyom Fyodorov
 */
@RepositoryRestResource(exported = false)
public interface InvoiceRepository extends PagingAndSortingRepository<Invoice, Long> {

    Optional<Invoice> findByOrder(Order order);

}
