package net.f.restclothing.business.orders.product;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

/**
 * @author Artyom Fyodorov
 */
@Component
@RequiredArgsConstructor class ProductLinks {
    static final String ORDER = "/order";
    static final String ORDER_SLASH_QUANTITY_SLASH_ADDRESS = "/order/quantity/{q:[1-9][0-9]*}/address/{a:[0-9][0-9]*}";
    static final String ORDER_REL = "product-order";

    private final @NonNull EntityLinks entityLinks;

    Link getOrderLink(Product product) {
        return entityLinks.linkForSingleResource(Product.class, product.getId())
                .slash(ORDER).withRel(ORDER_REL);
    }
}
