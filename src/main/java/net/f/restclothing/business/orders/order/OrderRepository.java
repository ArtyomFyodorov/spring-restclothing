package net.f.restclothing.business.orders.order;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Optional;

/**
 * @author Artyom Fyodorov
 */
public interface OrderRepository extends PagingAndSortingRepository<Order, Long> {

    @RestResource(exported = false)
    Page<Order> findByCustomerId(Long customerId, Pageable pageable);

    @RestResource(exported = false)
    Optional<Order> findByCustomerIdAndStatus(Long customerId, OrderStatus status);

    @PreAuthorize("isAuthenticated()")
    @Query("SELECT o FROM Order o WHERE o.id = ?1 AND o.customerId = ?#{principal?.id}")
    @Override
    Optional<Order> findById(Long aLong);

    @RestResource(exported = false)
    Optional<Order> findByIdAndCustomerId(Long orderId, Long customerId);

    @PreAuthorize("isAuthenticated()")
    @Query("SELECT o FROM Order o WHERE o.customerId = ?#{principal?.id}")
    @Override
    Page<Order> findAll(Pageable pageable);

    @RestResource(exported = false)
    @Override
    Iterable<Order> findAll(Sort sort);

    @Override
    <S extends Order> S save(S s);

    @RestResource(exported = false)
    @Override
    <S extends Order> Iterable<S> saveAll(Iterable<S> iterable);

    @RestResource(exported = false)
    @Override
    boolean existsById(Long aLong);

    @RestResource(exported = false)
    @Override
    Iterable<Order> findAll();

    @RestResource(exported = false)
    @Override
    Iterable<Order> findAllById(Iterable<Long> iterable);

    @RestResource(exported = false)
    @Override
    long count();

    @RestResource(exported = false)
    @Override
    void deleteById(Long aLong);

    @RestResource(exported = false)
    @Override
    void delete(Order order);

    @RestResource(exported = false)
    @Override
    void deleteAll(Iterable<? extends Order> iterable);

    @RestResource(exported = false)
    @Override
    void deleteAll();
}
