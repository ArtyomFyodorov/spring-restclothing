package net.f.restclothing.business.orders.product;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.f.restclothing.business.orders.order.OrderService;
import net.f.restclothing.business.security.CustomUserDetails;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Artyom Fyodorov
 */
@Controller
@RequestMapping("/products/{id}")
@RequiredArgsConstructor
public class ProductController {

    private final @NonNull OrderService orderService;

    @PostMapping(path = ProductLinks.ORDER)
    public ResponseEntity<?> makeOrder(@PathVariable("id") Product product,
                                       @AuthenticationPrincipal CustomUserDetails userDetails) {
        return makeOrder(product, 1, 0, userDetails);
    }

    @PostMapping(path = ProductLinks.ORDER_SLASH_QUANTITY_SLASH_ADDRESS)
    public ResponseEntity<?> makeOrder(@PathVariable("id") Product product,
                                       @PathVariable("q") int quantity,
                                       @PathVariable("a") int indexOfAddress,
                                       @AuthenticationPrincipal CustomUserDetails userDetails) {

        if (product == null) {
            return ResponseEntity.notFound().build();
        }
        // An additional check that the userDetails object is not null.
        // This check is not necessary, see the SecurityConfig class.
        if (userDetails == null || userDetails.getId() == null) {
            throw new UsernameNotFoundException("User is not found");
        }

        orderService.makeOrder(product, quantity, indexOfAddress, userDetails.getId());

        return ResponseEntity.noContent().build();
    }

}
