package net.f.restclothing.business.customers.address;

/**
 * @author Artyom Fyodorov
 */
public enum Country {
    RUSSIAN_FEDERATION, UNITED_STATES;
}
