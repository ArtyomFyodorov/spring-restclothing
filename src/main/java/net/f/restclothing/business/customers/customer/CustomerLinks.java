package net.f.restclothing.business.customers.customer;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.f.restclothing.business.orders.order.Order;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

/**
 * @author Artyom Fyodorov
 */
@Component
@RequiredArgsConstructor
public class CustomerLinks {
    public static final String UPDATE_REL = "update";
    public static final String ORDERS_REL = "orders";

    private final @NonNull EntityLinks entityLinks;

    public Link getUpdateLink(Customer customer) {
        return entityLinks.linkForSingleResource(Customer.class, customer.getId())
                .withRel(UPDATE_REL);
    }

    public Link getOrdersLink() {
        return entityLinks.linkToCollectionResource(Order.class)
                .withRel(ORDERS_REL);
    }
}
