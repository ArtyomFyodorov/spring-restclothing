package net.f.restclothing.business.customers.address;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

/**
 * @author Artyom Fyodorov
 */
@RepositoryRestResource(exported = false)
public interface AddressRepository extends CrudRepository<Address, Long> {

    @PreAuthorize("isAuthenticated()")
    @Query("SELECT c.addresses FROM Customer c WHERE c.id = ?#{principal?.id}")
    List<Address> findAddressesOfCurrentCustomer();
}
