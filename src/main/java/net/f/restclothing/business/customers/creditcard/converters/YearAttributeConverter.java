package net.f.restclothing.business.customers.creditcard.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.Year;

/**
 * @author Artyom Fyodorov
 */
@Converter(autoApply = true)
public class YearAttributeConverter implements AttributeConverter<Year, Integer> {

    public Integer convertToDatabaseColumn(Year toConvert) {
        return toConvert == null ? null : toConvert.getValue();
    }

    public Year convertToEntityAttribute(Integer toConvert) {
        return toConvert == null ? null : Year.of(toConvert);
    }
}
