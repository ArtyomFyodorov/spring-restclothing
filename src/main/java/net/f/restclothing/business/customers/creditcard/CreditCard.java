package net.f.restclothing.business.customers.creditcard;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.f.restclothing.business.base.AbstractEntity;
import net.f.restclothing.business.customers.creditcard.constraints.CreditCardNumber;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.NUMBER;
import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;
import static java.lang.Boolean.FALSE;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "credit_cards")
@Getter
@NoArgsConstructor(force = true)
@ToString(callSuper = true)
public class CreditCard extends AbstractEntity {

    @Column(name = "card_number")
    private final @CreditCardNumber String cardNumber;

    @JsonProperty(access = WRITE_ONLY)
    @Column(name = "holder_name")
    private final @NotBlank String cardHolderName;

    @JsonFormat(shape = NUMBER)
    @JsonProperty(access = WRITE_ONLY)
    @Column(name = "expiry_month")
    private final @NotNull Month expiryMonth;

    @JsonProperty(access = WRITE_ONLY)
    @Column(name = "expiry_year")
    private final @NotNull Year expiryYear;

    @JsonProperty(access = WRITE_ONLY)
    private final @Enumerated(EnumType.STRING) @NotNull CreditCardType type;

    private CreditCard(String cardNumber, String cardHolderName, Month expiryMonth, Year expiryYear, CreditCardType type) {
        this.cardNumber = cardNumber.replaceAll("-", "").replaceAll(" ", "");
        this.cardHolderName = cardHolderName;
        this.expiryMonth = expiryMonth;
        this.expiryYear = expiryYear;
        this.type = type;
    }

    public static CreditCard create(String cardNumber, String cardHolderName, Month expiryMonth, Year expiryYear, CreditCardType type) {
        return new CreditCard(cardNumber, cardHolderName, expiryMonth, expiryYear, type);
    }

    @JsonIgnore
    public boolean isValid() {
        return isValid(LocalDate.now());
    }

    @JsonIgnore
    public boolean isValid(LocalDate date) {
        return date == null ? FALSE : getExpirationDate().isAfter(date);
    }

    @JsonIgnore
    public LocalDate getExpirationDate() {
        return LocalDate.of(expiryYear.getValue(), expiryMonth, 1);
    }
}
