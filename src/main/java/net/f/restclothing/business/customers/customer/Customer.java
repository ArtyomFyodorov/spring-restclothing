package net.f.restclothing.business.customers.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.f.restclothing.business.base.AbstractEntity;
import net.f.restclothing.business.customers.address.Address;
import net.f.restclothing.business.customers.creditcard.CreditCard;
import net.f.restclothing.business.customers.customer.constraints.Email;
import net.f.restclothing.business.customers.customer.constraints.Password;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "customers")
@Getter @Setter
@NoArgsConstructor
@ToString(callSuper = true, exclude = "password")
public class Customer extends AbstractEntity {
    private static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    private @NotBlank String firstName;
    private @NotBlank String lastName;
    @Column(unique = true)
    private @Email String email;
    private @Password @JsonProperty(access = WRITE_ONLY) String password;

    @OneToOne(cascade = ALL)
    private @Valid CreditCard creditCard;
    @OneToMany(cascade = ALL, fetch = EAGER)
    private @Valid List<Address> addresses = new ArrayList<>();

    public Customer(String firstName, String lastName, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = PASSWORD_ENCODER.encode(password);
    }

    public void addAddress(Address address) {
        if (addresses == null) {
            addresses = new ArrayList<>();
        }
        this.addresses.add(address);
    }

    public void setPassword(String password) {
        this.password = PASSWORD_ENCODER.encode(password);
    }
}
