package net.f.restclothing.business.customers.customer;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Optional;

/**
 * @author Artyom Fyodorov
 */
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {

    @RestResource(exported = false)
    Optional<Customer> findByEmail(String name);

    @PreAuthorize("isAuthenticated()")
    @Query("SELECT c FROM Customer c WHERE c.email = :#{ principal?.username }")
    @RestResource(rel = "current-customer", path = "current-customer")
    Customer findMyself();

    @PreAuthorize("isAuthenticated()")
    @PostAuthorize("hasRole('ROLE_ADMIN') or returnObject.orElse(null)?.email == principal?.username")
    @Override
    Optional<Customer> findById(Long aLong);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Override
    Page<Customer> findAll(Pageable pageable);

    @Override
    <S extends Customer> S save(S s);

    @RestResource(exported = false)
    @Override
    void deleteById(Long aLong);

    @RestResource(exported = false)
    @Override
    Iterable<Customer> findAll(Sort sort);

    @RestResource(exported = false)
    @Override
    <S extends Customer> Iterable<S> saveAll(Iterable<S> iterable);

    @RestResource(exported = false)
    @Override
    boolean existsById(Long aLong);

    @RestResource(exported = false)
    @Override
    Iterable<Customer> findAll();

    @RestResource(exported = false)
    @Override
    Iterable<Customer> findAllById(Iterable<Long> iterable);

    @RestResource(exported = false)
    @Override
    long count();

    @RestResource(exported = false)
    @Override
    void delete(Customer customer);

    @RestResource(exported = false)
    @Override
    void deleteAll(Iterable<? extends Customer> iterable);

    @RestResource(exported = false)
    @Override
    void deleteAll();
}
