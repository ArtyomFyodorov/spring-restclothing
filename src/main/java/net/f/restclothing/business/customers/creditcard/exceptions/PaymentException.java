package net.f.restclothing.business.customers.creditcard.exceptions;

import lombok.Getter;
import net.f.restclothing.business.orders.order.Order;
import org.springframework.util.Assert;

/**
 * @author Artyom Fyodorov
 */
@Getter
public class PaymentException extends RuntimeException {

    private static final long serialVersionUID = 7813676287761333858L;
    private Order order;

    public PaymentException(Order order, String message) {
        super(message);

        Assert.notNull(order, "Order may not be null");
        this.order = order;
    }
}
