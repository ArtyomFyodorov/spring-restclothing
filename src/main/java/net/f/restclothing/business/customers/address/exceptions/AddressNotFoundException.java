package net.f.restclothing.business.customers.address.exceptions;

/**
 * @author Artyom Fyodorov
 */
public class AddressNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 7135392178716026584L;

    public AddressNotFoundException(String message) {
        super(message);
    }
}
