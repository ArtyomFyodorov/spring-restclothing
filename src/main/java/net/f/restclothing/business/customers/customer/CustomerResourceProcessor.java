package net.f.restclothing.business.customers.customer;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

/**
 * @author Artyom Fyodorov
 */
@Component
@RequiredArgsConstructor
public class CustomerResourceProcessor implements ResourceProcessor<Resource<Customer>> {

    private final @NonNull CustomerLinks customerLinks;

    @Override
    public Resource<Customer> process(Resource<Customer> customerResource) {
        final Customer customer = customerResource.getContent();
        customerResource.add(customerLinks.getUpdateLink(customer));
        customerResource.add(customerLinks.getOrdersLink());

        return customerResource;
    }
}
