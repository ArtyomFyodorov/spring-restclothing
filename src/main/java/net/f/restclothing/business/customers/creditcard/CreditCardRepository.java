package net.f.restclothing.business.customers.creditcard;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author Artyom Fyodorov
 */
@RepositoryRestResource(exported = false)
public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {
}
