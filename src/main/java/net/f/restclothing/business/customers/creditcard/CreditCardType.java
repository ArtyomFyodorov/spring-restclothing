package net.f.restclothing.business.customers.creditcard;

/**
 * @author Artyom Fyodorov
 */
public enum CreditCardType {
    VISA, MASTERCARD, MIR
}
