package net.f.restclothing.business.customers.address;

import lombok.*;
import net.f.restclothing.business.base.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "addresses")
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class Address extends AbstractEntity {

    private @NotBlank String street;
    private @NotBlank String city;
    private String state;
    @NotNull(message = "{net.f.constraints.Country.NotNull.message}")
    @Enumerated(EnumType.STRING)
    private Country country;
    private @NotNull Integer zipCode;
}
